<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "украшения из золота, кольца золотые, серьги золотые, цепочки золотые, браслеты золотые, подвески из золота, запонки из золота, ювелирная мастерская, ювелирная мастерская в москве, мастерская ювелирных изделий,ювелирные мастерские цены, ремонт ювелирных украшений, изготовление ювелирных украшений, переплавка золота,переплавка золота цена, адреса ювелирных мастерских, изготовление ювелирных изделий,кольца на заказ, обручальные кольца на заказ, изготовление колец на заказ, кольцо на заказ цена, сделать кольцо на заказ,мастерская ювелирных изделий,ремонт золота, ремонт цепочек,ремонт золотых цепочек,ремонт ювелирных изделий, ремонт ювелирных изделий в москве, ремонт ювелирных украшений");
$APPLICATION->SetPageProperty("keywords_inner", "украшения из золота, кольца золотые, серьги золотые, цепочки золотые, браслеты золотые, подвески из золота, запонки из золота, ювелирная мастерская, ювелирная мастерская в москве, мастерская ювелирных изделий,ювелирные мастерские цены, ремонт ювелирных украшений, изготовление ювелирных украшений, переплавка золота,переплавка золота цена, адреса ювелирных мастерских, изготовление ювелирных изделий,кольца на заказ, обручальные кольца на заказ, изготовление колец на заказ, кольцо на заказ цена, сделать кольцо на заказ,мастерская ювелирных изделий,ремонт золота, ремонт цепочек,ремонт золотых цепочек,ремонт ювелирных изделий, ремонт ювелирных изделий в москве, ремонт ювелирных украшений");
$APPLICATION->SetPageProperty("keywords", "Переплавка золота, Украшения из золота, Кольца золотые,Серьги золотые,Цепочки золотые,Браслеты золотые,Подвески из золота,Запонки из золота.");
$APPLICATION->SetPageProperty("description", "Ювелирные украшения на заказ, Кольца,Серьги,Цепи, Браслеты по нашим эскизам и вашим фотографиям");
$APPLICATION->SetTitle("Ювелирные украшения на заказ, Кольца,Серьги,Цепи, Браслеты по нашим эскизам и вашим фотографиям");
?> <?$APPLICATION->IncludeComponent(
	"bitrix:catalog",
	"template1",
	Array(
		"PATH_TO_SHIPPING" => "#SITE_DIR#about/delivery/",
		"DISPLAY_IMG_WIDTH" => "75",
		"DISPLAY_IMG_HEIGHT" => "225",
		"DISPLAY_DETAIL_IMG_WIDTH" => "350",
		"DISPLAY_DETAIL_IMG_HEIGHT" => "1000",
		"DISPLAY_MORE_PHOTO_WIDTH" => "50",
		"DISPLAY_MORE_PHOTO_HEIGHT" => "50",
		"SHARPEN" => "30",
		"AJAX_MODE" => "N",
		"SEF_MODE" => "Y",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "3",
		"USE_FILTER" => "N",
		"USE_REVIEW" => "N",
		"USE_COMPARE" => "N",
		"SHOW_TOP_ELEMENTS" => "N",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_TOP_DEPTH" => "2",
		"PAGE_ELEMENT_COUNT" => "15",
		"LINE_ELEMENT_COUNT" => "3",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "sort",
		"ELEMENT_SORT_ORDER2" => "asc",
		"LIST_PROPERTY_CODE" => array("ORPRICE"),
		"INCLUDE_SUBSECTIONS" => "Y",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"DETAIL_PROPERTY_CODE" => array("WEIGHT","ORPRICE","INSERT_TYPE","INSERT_COLOR","INSERT_CLARITY","INSERT_WEIGHT","METAL_TYPE","METAL_PURITY","LENGTH","PLETENIE","SIZE","MORE_PHOTO"),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "-",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "Y",
		"PRICE_CODE" => array("BASE"),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_PROPERTIES" => array(),
		"USE_PRODUCT_QUANTITY" => "N",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"USE_ALSO_BUY" => "N",
		"USE_STORE" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"PAGER_TEMPLATE" => "arrows",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
		"PAGER_SHOW_ALL" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"CONVERT_CURRENCY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"SEF_FOLDER" => "/catalog/",
		"SEF_URL_TEMPLATES" => Array(
			"sections" => "/catalog/",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			"compare" => "compare/"
		),
		"VARIABLE_ALIASES" => Array(
			"sections" => Array(),
			"section" => Array(),
			"element" => Array(),
			"compare" => Array(),
		)
	)
);?> <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>