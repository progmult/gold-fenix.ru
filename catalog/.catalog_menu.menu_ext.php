<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

$aMenuLinksExt=$APPLICATION->IncludeComponent(
	"custom:menu.sections",
	"",
	Array(
		"IS_SEF" => "Y",
		"SEF_BASE_URL" => "/catalog/",
		"SECTION_PAGE_URL" => "#SECTION_CODE#/",
		"DETAIL_PAGE_URL" => "#SECTION_CODE#/#ELEMENT_CODE#/",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "3",
		"DEPTH_LEVEL" => "2",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "0"
	),
	false
);

/*
$aMenuLinksExt=$APPLICATION->IncludeComponent(
	"custom:menu.sections.fixed",
	"",
	Array(
		"IS_SEF" => "Y",
		"SEF_BASE_URL" => "/catalog/",
		"SECTION_PAGE_URL" => "#SECTION_CODE#/",
		"DETAIL_PAGE_URL" => "#SECTION_CODE#/#ELEMENT_CODE#",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "3",
		"DEPTH_LEVEL" => "1",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "0"
	),
	false
);
*/
	
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>