<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$time_start = microtime(true);

	//Находим активный раздел
	$active_section = '';
	$arURI = explode('/', $_SERVER['REQUEST_URI']);
	//echo '<pre>'; print_r($arURI); echo '</pre>';
	//echo '<pre>'; print_r($_GET); echo '</pre>';
	foreach($arURI as $key => $sect)
	{
		if(($sect == 'catalog') || ($sect == 'magazin'))
		{
			if(isset($arURI[$key + 1]) && strpos($arURI[$key + 1], '?') === FALSE)
				$active_section = $arURI[$key + 1];
		}
	}
	//Для фильтра
	if(isset($_GET['arrFilter_ff']['SECTION_ID']) && (int)$_GET['arrFilter_ff']['SECTION_ID'] > 0)
	{
		CModule::IncludeModule("iblock");
		$rsSectionByID = CIBlockSection::GetByID((int)$_GET['arrFilter_ff']['SECTION_ID']);
		if($arSectionByID = $rsSectionByID->Fetch())
		{
			$bFiltered = true;
			$active_section = $arSectionByID['CODE'];
		}
	}
	
	
	$time_end2 = microtime(true);
	echo '<!--  ';
	echo $active_section.' ';
	echo $time_end2-$time_start;
	echo ' -->';

if(empty($active_section)) {
	$arParams["DEPTH_LEVEL"]=1;
}
	//Получаем подразделы
	//"ВСЕ ЧТО ЛИ? ВСЕ 1000 ШТУК? А потом у нас сайт тормозит..."	
	//$rsSections = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], ">DEPTH_LEVEL" => 1));
	//while($arSection = $rsSections->GetNext())
	//{
	//	$arSubSections[$arSection['IBLOCK_SECTION_ID']][] = $arSection['CODE'];
	//}
	//"ага, все деревце. только, чтобы выделить активный элемент ниже по коду... пока закомментировал".
	//echo '<!--  ';
	//echo print_r($arSubSections);
	//echo ' -->';

if($arParams["IS_SEF"] === "Y")
{
	if(($url = strstr($_SERVER['REQUEST_URI'], $arParams['SEF_BASE_URL'])) !== FALSE)
	{
		//Отсекаем GET-параметры
		if(($clear_url = strstr($url, '?', true)) !== FALSE)
			$url = $clear_url;
		//Получаем массив-путь
		$arUrl = explode('/', $url);
		
		foreach($arUrl as $key => $value)
		{
			if(strlen($value) <= 0)
			{
				unset($arUrl[$key]);
			}
			else
				$arResult['SEF']['PATH'][] = trim($value);
		}
		
		//Получаем тип страницы(list, section, detail) компонента
		if(count($arResult['SEF']['PATH']) == 2)
		{
			$arResult['SEF']['PAGE_TYPE'] = 'section';
		}
		elseif(count($arResult['SEF']['PATH']) == 3)
		{
			$arResult['SEF']['PAGE_TYPE'] = 'detail';
		}
		$arResult['SEF']['SECTION_CODE'] = $arResult['SEF']['PATH'][1];
	}
	if($bFiltered)
	{
		$arResult['SEF']['SECTION_CODE'] = $active_section;
	}
	//echo '<pre>'; print_r($arResult['SEF']); echo '</pre>';
}
$arParams["ID"] = intval($arParams["ID"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["DEPTH_LEVEL"] = intval($arParams["DEPTH_LEVEL"]);
if($arParams["DEPTH_LEVEL"]<=0)
	$arParams["DEPTH_LEVEL"]=1;

$arResult["SECTIONS"] = array();
$arResult["ELEMENT_LINKS"] = array();

if($this->StartResultCache())
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
	}
	else
	{
		$arFilter = array(
			"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
			"GLOBAL_ACTIVE"=>"Y",
			"IBLOCK_ACTIVE"=>"Y",
			"<="."DEPTH_LEVEL" => $arParams["DEPTH_LEVEL"],
		);
		
		//Разделы только второго уровня
		
		$arrFilter = array();
		if(!empty($arResult['SEF']['SECTION_CODE']))
		{
			$rsParentSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arResult['SEF']['SECTION_CODE']));
			if($arParentSection = $rsParentSection->GetNext())
			{
				if($arParentSection['DEPTH_LEVEL'] > 1)
				{
					$arrFilter['=SECTION_ID'] = $arParentSection['IBLOCK_SECTION_ID'];
					
					$rsParentSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "=ID" => $arParentSection['IBLOCK_SECTION_ID']));
					$arParentSection = $rsParentSection->GetNext();
				}	
				else
					$arrFilter['=SECTION_ID'] = $arParentSection['ID'];
			}
		}
		//Заголовок для внутренних подразделов
		if(($active_section == $arParentSection['CODE']))
			$parent_active_section = 1;
		else
			$parent_active_section = 0;
		$arResult["SECTIONS"][] = array(
			"ID" => 0,
			"DEPTH_LEVEL" => 0,
			"~NAME" => $arParentSection['NAME'],
			"IS_HEADER_ITEM" => 1,
			'LINK' => $arParentSection['SECTION_PAGE_URL'],
			"ACTIVE_SECTION" => $parent_active_section
		);
		$arResult["ELEMENT_LINKS"][$arSection["ID"]] = array();
		//
		
		
		$arOrder = array(
			"left_margin"=>"asc",
		);

		$rsSections = CIBlockSection::GetList($arOrder, array_merge($arrFilter, $arFilter), false, array(
			"ID",
			"DEPTH_LEVEL",
			"NAME",
			"SECTION_PAGE_URL",
			"UF_*",
		));
		if($arParams["IS_SEF"] !== "Y")
			$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		else
			$rsSections->SetUrlTemplates("", $arParams["SEF_BASE_URL"].$arParams["SECTION_PAGE_URL"]);
		while($arSection = $rsSections->GetNext())
		{
			if(($active_section == $arSection['CODE']) || (in_array($active_section, $arSubSections[$arSection['ID']])))
				$arSection['ACTIVE_SECTION'] = 1;
			else
				$arSection['ACTIVE_SECTION'] = 0;
			$arResult["SECTIONS"][] = array(
				"ID" => $arSection["ID"],
				"CODE" => $arSection["CODE"],
				"SECT_URI" => array($arURI, $active_section),
				"ACTIVE_SECTION" => $arSection['ACTIVE_SECTION'],
				"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
				"~NAME" => $arSection["~NAME"],
				"~SECTION_PAGE_URL" => $arSection["~SECTION_PAGE_URL"],
				"UF_LINK" => $arSection['UF_LINK'],
			);
			$arResult["ELEMENT_LINKS"][$arSection["ID"]] = array();
		}
		$this->EndResultCache();
	}
}

//In "SEF" mode we'll try to parse URL and get ELEMENT_ID from it
if($arParams["IS_SEF"] === "Y")
{
	$componentPage = CComponentEngine::ParseComponentPath(
		$arParams["SEF_BASE_URL"],
		array(
			"section" => $arParams["SECTION_PAGE_URL"],
			"detail" => $arParams["DETAIL_PAGE_URL"],
		),
		$arVariables
	);
	if($componentPage === "detail")
	{
		CComponentEngine::InitComponentVariables(
			$componentPage,
			array("SECTION_ID", "ELEMENT_ID"),
			array(
				"section" => array("SECTION_ID" => "SECTION_ID"),
				"detail" => array("SECTION_ID" => "SECTION_ID", "ELEMENT_ID" => "ELEMENT_ID"),
			),
			$arVariables
		);
		$arParams["ID"] = intval($arVariables["ELEMENT_ID"]);
	}
}

if(($arParams["ID"] > 0) && (intval($arVariables["SECTION_ID"]) <= 0) && CModule::IncludeModule("iblock"))
{
	$arSelect = array("ID", "IBLOCK_ID", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID");
	$arFilter = array(
		"ID" => $arParams["ID"],
		"ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);
	$rsElements = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	if(($arParams["IS_SEF"] === "Y") && (strlen($arParams["DETAIL_PAGE_URL"]) > 0))
		$rsElements->SetUrlTemplates($arParams["SEF_BASE_URL"].$arParams["DETAIL_PAGE_URL"]);
	while($arElement = $rsElements->GetNext())
	{
		$arResult["ELEMENT_LINKS"][$arElement["IBLOCK_SECTION_ID"]][] = $arElement["~DETAIL_PAGE_URL"];
	}
}

$aMenuLinksNew = array();
$menuIndex = 0;
$previousDepthLevel = 1;
//echo '<pre>'; print_r($arResult["SECTIONS"]); echo '</pre>';die();
foreach($arResult["SECTIONS"] as $arSection)
{
	if ($menuIndex > 0)
		$aMenuLinksNew[$menuIndex - 1][3]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
	$previousDepthLevel = $arSection["DEPTH_LEVEL"];
	
	//Альтернативная ссылка
	if(!empty($arSection['UF_LINK']))
		$link = $arSection['UF_LINK'];
	else
		$link = $arSection["~SECTION_PAGE_URL"];
	//
	
	$aMenuLinksNew[$menuIndex++] = array(
		htmlspecialchars($arSection["~NAME"]),
		$link,
		$arResult["ELEMENT_LINKS"][$arSection["ID"]],
		array(
			"FROM_IBLOCK" => true,
			"IS_PARENT" => false,
			"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
			"IS_HEADER_ITEM" => (int)$arSection['IS_HEADER_ITEM'],
			'LINK' => $arSection['LINK'],
			"ACTIVE_SECTION" => $arSection['ACTIVE_SECTION'],
			"SECT_URI" => $arSection['SECT_URI'],
			"CODE" => $arSection["CODE"],
			//'TEST' => $_SERVER['REQUEST_URI'],
			//'TEST1' => explode('/', $_SERVER['REQUEST_URI'])
		),
	);
}

return $aMenuLinksNew;
?>
