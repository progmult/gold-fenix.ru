<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<script>
$(document).ready(function() {
$('#popupbutton').fancybox({
		'type' : 'inline',
		'overlayShow': false,
        'padding': 37,
        'overlayOpacity': 0.87,
        'overlayColor': '#fff',
        'transitionIn': 'FadeIn',
        'transitionOut': 'FadeOut',
        'titlePosition': 'inside',
        'centerOnScroll': true,
        'autoSize': true,
        'width' : 'auto',
        'maxWidth' : '100%'		
    });
});	

$(document).ready(function() {
$('#open_modal').fancybox({
		'type' : 'inline',
		'overlayShow': false,
        'padding': 37,
        'overlayOpacity': 0.87,
        'overlayColor': '#fff',
        'transitionIn': 'FadeIn',
        'transitionOut': 'FadeOut',
        'titlePosition': 'inside',
        'centerOnScroll': true,
        'autoSize': true,
        'width' : 'auto',
        'maxWidth' : '100%'		
    });
});		
function orderk($idd){
	$('#product_ch input.inputtext').val($idd);
}

</script>

<div style="display: none;"> 

  <div id="popupform"> <span style="font-size: 26px;">Заказать</span>  
  
  <?$APPLICATION->IncludeComponent(
	"bitrix:form",
	"",
	Array(
		"AJAX_MODE" => "Y",
		"SEF_MODE" => "N",
		"WEB_FORM_ID" => "1",
		"RESULT_ID" => $_REQUEST[RESULT_ID],
		"START_PAGE" => "new",
		"SHOW_LIST_PAGE" => "N",
		"EMAIL_TO" => "glg-kd@mail.ru",	
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_VIEW_PAGE" => "N",
		"SUCCESS_URL" => "",
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_STATUS" => "Y",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "N",
		"NOT_SHOW_FILTER" => array(),
		"NOT_SHOW_TABLE" => array(),
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"VARIABLE_ALIASES" => Array(
			"action" => "action"
		)
	)
);?> 

  </div>
 </div>
 <script>$('#popupform input[type="submit"]').attr('onclick','yaCounter31249683.reachGoal ("form_send"); return true;');
 $('.bck_form input[type="submit"]').attr('onclick','yaCounter31249683.reachGoal ("form_send"); return true;');</script>
<?php
	
	if(strlen($APPLICATION->GetPageProperty("social_buttons")) > 0)
	{
		$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
			"AREA_FILE_SHOW" => "file", 
			"PATH" => SITE_TEMPLATE_PATH."/include/social_buttons.php"), 
			false
		);
	}
?>
</div>
</div>
        <!-- END main -->
        <div class="clear"></div>

        <!-- Footer -->
        <footer class="wrapp"> 
        	<div class="footer-catalog">
			<?php include($_SERVER['DOCUMENT_ROOT'].'/include/left_menu.php');?>
        	</div>

            <a id="footer-logo" href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/footer-logo.png" width="141" height="37" alt=""/></a>
            <div class="copy"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/copyright.php"), false);?></div>
            <div class="text">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/copyright_extra.php"), false);?>
            </div>

			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => "/include/social_icons.php",
				"EDIT_TEMPLATE" => ""
				),
				false
			);?>
			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => "/include/counters.php",
				"EDIT_TEMPLATE" => ""
				),
				false
			);?>
        </footer>
        <!-- END Footer -->

    </div>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.uniform.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/cart.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.anythingslider.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.cycle.all.js');?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tipTip.minified.js');?>

    <script type="text/javascript">
      $(function(){		
		$("a.social-icon").tipTip({
			delay:50
		});
			$("#clear-button").click(function(){
				$(this).closest(".header-search").find("input[type=text]").val('');
				$(this).fadeOut("fast");
				return false;
			});
			
			$("#qg").keyup(function(){
				var search_val = $(this).val();
				if(parseInt(search_val.length) <= 0)
				{
					$("#clear-button").fadeOut("fast");
				}
				else
				{
					$("#clear-button").fadeIn("fast");
				}
			});
			
			$("#qg").keyup();

		$('#products_slider').cycle({ 
			next:   '#right_arrow',
			prev:   '#left_arrow',				
			fx:      'scrollLeft', 
			timeout:  0,
			width:'645px',
			height: '160px'
		});
			
        $(".header-menu input, .block-filter input, textarea, select").not("#order_form_id select, .not-uniform").addClass("uniformed").uniform();
		
		<?php
			$left = 0;
			$right = 150;

			if (isset($_GET['arrFilter_pf']['LENGTH']['LEFT']) && $_GET['arrFilter_pf']['LENGTH']['LEFT'] !== '')
			{
				$left = (int)$_GET['arrFilter_pf']['LENGTH']['LEFT'];
			}
			if (isset($_GET['arrFilter_pf']['LENGTH']['RIGHT']) && $_GET['arrFilter_pf']['LENGTH']['RIGHT'] !== '')
			{
				$right = (int)$_GET['arrFilter_pf']['LENGTH']['RIGHT'];
			}
		?>
		
        $( "#progresbar" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ <?=$left?>, <?=$right?> ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
				
				$( "#arr_left" ).val(ui.values[ 0 ]);
				$( "#arr_right" ).val(ui.values[ 1 ]);
				
				//Активация бегунков размера
				var section_id = $('select[name="arrFilter_ff[SECTION_ID]"]').val();
				var unset_size_filter = false;
				
				if(section_id == 28)

				{
					if(ui.values[ 0 ] == 15 && ui.values[ 1 ] == 25)
							unset_size_filter = true;
				}
				else if(section_id == 31)
				{
					if(ui.values[ 0 ] == 15 && ui.values[ 1 ] == 80)
						unset_size_filter = true;
				}
				else if(section_id == 32)
				{
					if(ui.values[ 0 ] == 5 && ui.values[ 1 ] == 24)
						unset_size_filter = true;
				}
				else if(section_id == 36)
				{
					if(ui.values[ 0 ] == 5 && ui.values[ 1 ] == 25)
						unset_size_filter = true;
				}
				
				if(unset_size_filter)
				{
					if($( "#arr_left" ).attr("name") == 'arrFilter_pf[LENGTH][LEFT]')
						$( "#arr_left" ).attr("name", 'not_active_arrFilter_pf[LENGTH][LEFT]'); 
					if($( "#arr_right" ).attr("name") == 'arrFilter_pf[LENGTH][RIGHT]')
						$( "#arr_right" ).attr("name", 'not_active_arrFilter_pf[LENGTH][RIGHT]');
				}
				else
				{
					if($( "#arr_left" ).attr("name") == 'not_active_arrFilter_pf[LENGTH][LEFT]')
						$( "#arr_left" ).attr("name", 'arrFilter_pf[LENGTH][LEFT]'); 
					if($( "#arr_right" ).attr("name") == 'not_active_arrFilter_pf[LENGTH][RIGHT]')
						$( "#arr_right" ).attr("name", 'arrFilter_pf[LENGTH][RIGHT]');
				}
            }
        });
		
        $( "#amount" ).val( "" + $( "#progresbar" ).slider( "values", 0 ) +
			" - " + $( "#progresbar" ).slider( "values", 1 ) );
		
		$( "#arr_left" ).val($( "#progresbar" ).slider( "values", 0 ));
		$( "#arr_right" ).val($( "#progresbar" ).slider( "values", 1 ));
			
		 $('#slider').anythingSlider();
		 
		<?/*var cx = '015652224943537250373:bq9estbm1ba';
		var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
		gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
			'//www.google.com/cse/cse.js?cx=' + cx;
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); */?>
    });
    </script> 
</body>
</html>