<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//echo '<pre>'; print_r($arResult); echo '</pre>';
if (count($arResult) < 1)
	return;

$bManyIblock = array_key_exists("IBLOCK_ROOT_ITEM", $arResult[0]["PARAMS"]);

if (strpos($_SERVER['REQUEST_URI'], 'personal') === false) {
	echo '<H2>Тип изделия</H2>';
}
?>


	<ul id="left-menu">
<?


	$previousLevel = 0;
	foreach($arResult as $key => $arItem):
/*
		if (!in_array(8, CUser::GetUserGroup($_SESSION['SESS_AUTH']['USER_ID'])) && strpos($arItem['LINK'], 'supplier') > -1) {
			continue;
		}

		if (in_array(8, CUser::GetUserGroup($_SESSION['SESS_AUTH']['USER_ID'])) && (strpos($arItem['LINK'], 'cart') > -1 || strpos($arItem['LINK'], 'order') > -1)) {
			continue;
		}
*/

		if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):
			//echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
		endif;

		if ($arItem["IS_PARENT"]):
			$i = $key;
			$bHasSelected = $arItem['SELECTED'];
			$childSelected = false;
			if (!$bHasSelected)
			{
				while ($arResult[++$i]['DEPTH_LEVEL'] > $arItem['DEPTH_LEVEL'])
				{
					if ($arResult[$i]['SELECTED'])
					{
						$bHasSelected = $childSelected = true; break;
					}
				}
			}
			
			$className = $nHasSelected ? 'selected' : '';//($bHasSelected ? 'selected' : '');
?>
		<? if ($arItem['DEPTH_LEVEL'] > 1 && !$childSelected && $bHasSelected) :?>
			<li class="current">
			<a class="selected" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
			<ul>

		<? else:?>
			<li<?=$bHasSelected ? ' class="selected"' : ''?>>
			<a<?=$bHasSelected ? ' class="selected"' : ''?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
			<ul<?=$bHasSelected || ($bManyIblock && $arItem['DEPTH_LEVEL'] <= 1) ? '' : ' style="display: none;"'?>>
		<? endif?>


<?
		else:
			if ($arItem["PERMISSION"] > "D"):
				$className = $arItem['SELECTED'] ? $arItem['DEPTH_LEVEL'] > 1 ? 'current' : "selected" : '';
?>
		<li<?=$className ? ' class="'.$className.'"' : ' class="l_mnu"'?>>

			<? if (!sectionIsEmpty(3, $arItem['LINK']) || strpos($_SERVER['REQUEST_URI'], 'personal') > -1): ?>
				<a<?if ($arItem['SELECTED']):?> class="selected"<?endif?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
			<? else: ?>
				<span style="color:#c1c1c1"><?=$arItem["TEXT"]?></span>
			<? endif; ?>

		</li>
<?
			endif;
		endif;

		$previousLevel = $arItem["DEPTH_LEVEL"];
	endforeach;

	if ($previousLevel > 1)://close last item tags
		//echo str_repeat("</ul></li>", ($previousLevel-1) );
	endif;
?>

	</ul>

<?php
if (strpos($_SERVER['REQUEST_URI'], 'personal') === false) {
	echo '<H2><a href="/catalog/?arrFilter_pf%5BBRAND%5D%5B%5D=4&set_filter=Найтиg">Брендовые изделия</a></H2>
';
}
?>



<?
$APPLICATION->IncludeComponent("bitrix:news.list", "sidebar", array(
	"IBLOCK_TYPE" => "news",
	"IBLOCK_ID" => "1",
	"NEWS_COUNT" => "3",
	"SORT_BY1" => "ACTIVE_FROM",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_SHADOW" => "Y",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"DISPLAY_PANEL" => "N",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
?>
