<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

//determine if child selected

$bWasSelected = false;
$arParents = array();
$depth = 1;
$arItems = $arResult;
$arResult = array();
$arResult['ITEMS'] = $arItems;

foreach($arResult['ITEMS'] as $i=>$arMenu)
{
	$depth = $arMenu['DEPTH_LEVEL'];

	if($arMenu['IS_PARENT'] == true)
	{
		$arParents[$arMenu['DEPTH_LEVEL']-1] = $i;
	}
	elseif($arMenu['SELECTED'] == true)
	{
		$bWasSelected = true;
		break;
	}
	
	if((int)$arMenu['PARAMS']['IS_HEADER_ITEM'] > 0)
	{
		$arResult['CATALOG_HEADER'] = $arMenu['TEXT'];
		$arResult['CATALOG_HEADER_LINK'] = $arMenu['PARAMS']['LINK'];
		$arResult['ITEMS'][$i]['IS_PARENT'] = '';
		$arResult['ITEMS'][$i]['LINK'] = $arMenu['PARAMS']['LINK'];
		//unset($arResult['ITEMS'][$i]);
	}
}
/*
if($bWasSelected)
{
	for($i=0; $i<$depth-1; $i++)
		$arResult['ITEMS'][$arParents[$i]]['CHILD_SELECTED'] = true;
}
*/
?>
