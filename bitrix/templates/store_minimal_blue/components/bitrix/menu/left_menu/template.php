<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="left-menu">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
	if ($arResult['SECTION']['ID']=="180") break;
?>
	
	<?if($arItem['PARAMS']['sub_menu_header'] == 'Y'):?>
		<li>
			<a rel="nofollow" class="sub_menu_header"><?=$arItem["TEXT"]?></a>
			<ul>
	<?else:?>
		<?if($arItem["SELECTED"]):?>
			<li><a<?if((int)$arItem['PARAMS']['follow'] <= 0):?> rel="nofollow"<?endif;?> href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
		<?else:?>
			<li><a<?if((int)$arItem['PARAMS']['follow'] <= 0):?> rel="nofollow"<?endif;?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
		<?endif?>	
	<?endif;?>
	<?if($arItem['PARAMS']['last_sub_menu_item'] == 'Y'):?>
			</ul>
		</li>
	<?endif;?>
	
<?endforeach?>

</ul>
<?endif?>
<script>
	jQuery(function($){
		$("ul.left-menu .sub_menu_header").click(function(){
			$(this).next('ul').slideToggle("fast");
		});
		if($("ul.left-menu .sub_menu_header").length > 0)
		{
			if($("ul.left-menu .sub_menu_header").next('ul').find("a.selected").length > 0)
				$("ul.left-menu .sub_menu_header").next('ul').show();
			else
				$("ul.left-menu .sub_menu_header").next('ul').hide();
		}
	});
</script>