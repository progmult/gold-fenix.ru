<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	//Перенаправление поставщика в личный кабинет при успешной авторизации
	if( $USER->IsAuthorized() && $_REQUEST['AUTH_FORM'] == 'Y')
	{
		if(IsUserSupplier())
		{
			LocalRedirect("/personal/");
		}
	}
?>

<?if(!empty($arResult['POST']['AUTH_FORM']) && !isset($_REQUEST['REGISTER'])):?>
	<script>
		jQuery(function($){
			$('#modal_login').dialog({
                autoOpen: false,
                draggable: false,
                modal: true,
                resizable: false,
                width: 325,
                //show: 'scale',
                //hide: 'scale'
            });
			
			 $('#modal_signup').dialog({
                autoOpen: false,
                draggable: false,
                modal: true,
                resizable: false,
                width: 460,
                //show: 'scale',
                //hide: 'scale'
            });
			
			$('#modal_login').dialog( "open" );
		});
	</script>
<?endif;?>
<?if($arResult["FORM_TYPE"] == "login"):?>
<div id="modal_login" style="display: none;">
	<div class="modal_close_button"></div>
	<div class="modal_tabs">
		<ul>
			<li class="disable">Авторизация</li>
			<?/*<li>Авторизация</li>*/?>
		</ul>
	</div>
	<div class="modal_body">
		<form id="login_form" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
			<?if($arResult["BACKURL"] <> ''):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>
			<?foreach ($arResult["POST"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
				<input type="hidden" name="AUTH_FORM" value="Y" />
				<input type="hidden" name="TYPE" value="AUTH" />
			<div class="row">
				<label for="USER_LOGIN">Электронная почта</label>
				<input type="text" name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>" />
			</div>
			<div class="row">
				<label for="USER_PASSWORD">Пароль</label>
				<input type="password" name="USER_PASSWORD" />
			</div>
			<div class="links">
				<div style="text-align: left"><a rel="nofollow" id="link_to_register" style="cursor:pointer;">Регистрация</a></div>
				<div style="text-align: right"><a rel="nofollow" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>">Забыли пароль?</a></div>
			</div>
			<input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" style="position:absolute; left:-9999px;" />
		</form>
		
		<?if(!empty($arResult['ERROR_MESSAGE']['MESSAGE'])):?>
			<p style="color:red;margin:0;padding: 5px 15px;"><?=$arResult['ERROR_MESSAGE']['MESSAGE']?></p>
		<?endif;?>
		
		<hr />
		<?if($arResult["AUTH_SERVICES"]):?>
		<div style="text-align: center; width: 200px; margin: 8px auto;">
			Авторизация / Регистрация через соц. сети
		</div>
		
		<div class="social mini">
			<?/*
			<a class="f" href="#"></a>
			<a class="t" href="#"></a>
			<a class="v" href="#"></a>
			<a class="o" href="#"></a>
			<a class="y" href="#"></a>
			*/?>
			<?$APPLICATION->IncludeComponent(
				"custom:auth",
				"",
				Array(
					"PROVIDERS" => "vkontakte,odnoklassniki,facebook,twitter,google",
					"HIDDEN" => "",
					"TYPE" => "panel",
					"REDIRECT_PAGE" => "",
					"UNIQUE_EMAIL" => "Y",
					"SEND_MAIL" => "N",
					"GROUP_ID" => array("7")
				),
			false
			);?>
		</div>
		<?endif;?>
	</div>
	<div class="modal_bottom">
		<a rel="nofollow" href="#" id="btn_submit">Вход</a>
		<script>
			$(document).ready(function(){
				$('#btn_submit').click(function(){
					$('#login_form').submit()
				})
			})
		</script>
	</div>
</div>
<?endif;?>