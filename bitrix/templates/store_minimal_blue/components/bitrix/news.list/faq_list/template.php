<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="faq-list">
	<?if(!empty($arResult["ITEMS"])):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?><br />
		<?endif;?>
		<?$i = 0;?>
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?$i++;?>
			<div class="faq-list-item<?if($i == count($arResult["ITEMS"])):?> faq-list-item-last<?endif;?>">
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="question"><?=$arItem['DETAIL_TEXT']?></div>
				<div class="answer"><?=$arItem['PREVIEW_TEXT']?></div>
			</div>
		<?endforeach;?>
		
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<br /><?=$arResult["NAV_STRING"]?>
		<?endif;?>
	<?else:?>
		<p class="faq-list-empty">�� ������ ������, ������ �������� ����.</p>
	<?endif;?>
</div>
