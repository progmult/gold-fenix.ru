<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult["ITEMS"])):?>
<div class="block-news">
	 <div class="title">Новости</div>
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="news">
				<div class="date"><?=$arItem['DISPLAY_ACTIVE_FROM']?></div>
				<a rel="nofollow" href="<?=$arItem['DETAIL_PAGE_URL']?>" class="name">
					<?=$arItem['NAME']?>
				</a>
			</div>
		<?endforeach;?>
</div>
<?endif;?>
