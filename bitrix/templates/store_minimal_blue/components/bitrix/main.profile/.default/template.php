<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
//echo "<pre>"; print_r($arResult); echo "</pre>";
//exit();
//echo "<pre>"; print_r($_SESSION); echo "</pre>";

?>
<?=ShowError($arResult["strProfileError"]);?>
<?

if ($arResult['DATA_SAVED'] == 'Y') {
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
	$arResult['DATA_SAVED'] = 'N';
	$_SESSION['profile_saved'] = 'success';
	header ('Location: /personal/profile/?saved=1');
}

if ($_GET['saved'] == 1) {
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
}

?>
<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
<input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
<input type="hidden" name="EMAIL" value=<?=$arResult["arUser"]["EMAIL"]?> />

<div class="content-form profile-form">
	<div class="fields">

		<div class="field field-lastname">	
			<label class="field-title"><?=GetMessage('LAST_NAME')?></label>
			<div class="form-input"><input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" /></div>
		</div>
		<div class="field field-firstname">
			<label class="field-title"><?=GetMessage('NAME')?></label>
			<div class="form-input"><input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" /></div>
		</div>
		<div class="field field-secondname">	
			<label class="field-title"><?=GetMessage('SECOND_NAME')?></label>
			<div class="form-input"><input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" /></div>
		</div>
	</div>
	<div class="fields">
		<div class="field field-password_new">	
			<label class="field-title"><?=GetMessage('NEW_PASSWORD_REQ')?></label>

			<div class="form-input"><input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" /></div>
			
		</div>
		<div class="field field-password_confirm">
			<label class="field-title"><?=GetMessage('NEW_PASSWORD_CONFIRM')?></label>
			<div class="form-input"><input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" /></div>
			
		</div>
	</div>
		<div class="field field-phone">
			<label class="field-title">Телефон</label>
			<div class="form-input"><input type="text" name="PERSONAL_PHONE" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title"><?=GetMessage('USER_CITY')?></label>
			<div class="form-input"><input type="text" name="PERSONAL_CITY" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" /></div>
		</div>
		<div class="field field-phone">
		<?php
			$APPLICATION->IncludeComponent("custom:metro", "", array(
				'iblock_id' => 9,
				'field_name' => 'UF_METRO',
				'val' => $arResult["arUser"]["UF_METRO"],
			));
		?> 

		</div>
		<div class="field field-phone">
			<label class="field-title"><?=GetMessage("USER_STREET")?></label>
			<div class="form-input"><input type="text" name="PERSONAL_STREET" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_STREET"]?>" /></div>
		</div>

<?php if (in_array(8, CUser::GetUserGroup($arResult['arUser']['ID']))): ?>
		<div class="field field-phone field-metro">
			<label class="field-title"><?=$arResult['USER_PROPERTIES']['DATA']['UF_SUBWAY']['EDIT_FORM_LABEL']?></label>
			<div class="form-input">
				<?$APPLICATION->IncludeComponent(
						"bitrix:system.field.edit",
						$arResult['USER_PROPERTIES']['DATA']['UF_SUBWAY']["USER_TYPE"]["USER_TYPE_ID"],
						array(
							"bVarsFromForm" => $arResult["bVarsFromForm"], 
							"arUserField" => $arResult['USER_PROPERTIES']['DATA']['UF_SUBWAY']), 
							null,
							array("HIDE_ICONS"=>"Y")
				);
				?>
			</div>
		</div>
		<div class="field field-phone">
			<label class="field-title">Название организации</label>
			<div class="form-input"><input type="text" name="WORK_COMPANY" maxlength="50" value="<?=$arResult["arUser"]["WORK_COMPANY"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">Должность</label>
			<div class="form-input"><input type="text" name="WORK_POSITION" maxlength="50" value="<?=$arResult["arUser"]["WORK_POSITION"]?>" /></div>
		</div>		
		<div class="field field-phone">
			<label class="field-title">Сайт</label>
			<div class="form-input"><input type="text" name="WORK_WWW" maxlength="50" value="<?=$arResult["arUser"]["WORK_WWW"]?>" /></div>
		</div>


		<div class="field field-phone">
			<label class="field-title">Банк</label>
			<div class="form-input"><input type="text" name="UF_BANK" maxlength="50" value="<?=$arResult["arUser"]["UF_BANK"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">Корр. счет</label>
			<div class="form-input"><input type="text" name="UF_ACCOUNT" maxlength="50" value="<?=$arResult["arUser"]["UF_ACCOUNT"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">БИК</label>
			<div class="form-input"><input type="text" name="UF_BIK" maxlength="50" value="<?=$arResult["arUser"]["UF_BIK"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">ИНН</label>
			<div class="form-input"><input type="text" name="UF_INN" maxlength="50" value="<?=$arResult["arUser"]["UF_INN"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">КПП</label>
			<div class="form-input"><input type="text" name="UF_KPP" maxlength="50" value="<?=$arResult["arUser"]["UF_KPP"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">Расч. счет</label>
			<div class="form-input"><input type="text" name="UF_ACCOUNT2" maxlength="50" value="<?=$arResult["arUser"]["UF_ACCOUNT2"]?>" /></div>
		</div>



<?php endif;?>
</div>

<?php
//print '<pre>';
//print_R ($arResult);
?>

<div class="content-form profile-form">
	<div class="button"><input name="save" value="<?=GetMessage("MAIN_SAVE")?>"class="input-submit" type="submit"></div>
</div>
</form>