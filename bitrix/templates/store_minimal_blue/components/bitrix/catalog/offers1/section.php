<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"DISPLAY_PANEL" => "N",
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],

		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	),
	$component
);?>
<br />
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">

<?
foreach($arResult["ITEMS"] as $arItem)
	if(array_key_exists("HIDDEN", $arItem))
		echo $arItem["INPUT"];

foreach ($arResult['ITEMS'] as $arItem) {
	// Собираем селект со списокм секций
	if ($arItem['INPUT_NAME'] == 'arrFilter_ff[SECTION_ID]') {
		$arItem['INPUT'] = explode ('<br>', $arItem['INPUT']);
		$find = array('<option value=""> </option>', 'Верхний уровень', ' . ');
		$replace = array('', 'Все изделия', '');
		$section = str_replace($find, $replace, $arItem['INPUT'][0]);
	}

	// Собираем селект со списокм металов
	if ($arItem['INPUT_NAME'] == 'arrFilter_pf[METAL_TYPE]') {
		$metal = str_replace('(все)', 'Все металы', $arItem['INPUT']);
	}

	// Собираем селект со списокм вставок
	if ($arItem['INPUT_NAME'] == 'arrFilter_pf[INSERT_TYPE]') {
		$insert = str_replace('(все)', 'Все вставки', $arItem['INPUT']);
	}

	// Собираем цену
	if ($arItem['NAME'] == 'Цена') {
		$price = $arItem['INPUT'];
	}

	// Собираем размер
	if ($arItem['NAME'] == 'Размер') {
		$size = $arItem['INPUT'];
	}

	// Собираем длину
	if ($arItem['NAME'] == 'Длинна') {
		$length = $arItem['INPUT'];
	}

	// Собираем чекбокс комплект
	if ($arItem['NAME'] == 'Товар в комплекте') {
		$set = str_replace('1</label>', 'Комплект</label>', $arItem['INPUT']);
	}
}
?>

<div class="catalog-item-filter<?if ($arResult['IS_FILTERED']):?> filter-active<?endif;?>" >
	<div class="catalog-item-filter-title"><span><a href="#" id="catalog_item_toogle_filter"><?=($arResult['IS_FILTERED']) ? 'Подбор изделий':'Подбор изделий'?></a></span></div>
	<div class="catalog-item-filter-body" id="catalog_item_filter_body"  style="display:block">
		<b class="r1"></b>
		<div class="catalog-item-filter-body-inner" style="background-color:white"> 
			<table cellspacing="0" class="catalog-item-filter" id="catalog_item_filter_table" style="width:560px">
			<tbody>
				<tr>
					<td>
						<?=$section?>
					</td>
					<td>
						<?=$metal?>
					</td>
					<td>
						<?=$insert?>
					</td>
					<td>
						Цена от <?=$price?>
					</td>
				</tr>
			</tbody>
			</table>
<div id="filterSize" style="display:<?=(strlen($_GET['arrFilter_pf']['SIZE']['LEFT']) > 0 || strlen($_GET['arrFilter_pf']['SIZE']['RIGHT']) > 0) ? 'block':'none'?>;">Размер <?=$size?></div>
<div id="filterLength" style="display:<?=(strlen($_GET['arrFilter_pf']['LENGTH']['LEFT']) > 0 || strlen($_GET['arrFilter_pf']['LENGTH']['RIGHT']) > 0) ? 'block':'none'?>;">Длинна <?=$length?></div>
<div style=""><?=$set?></div>




		</div>
			<div style="margin-left:380px;width:150px;margin-top:30px;margin-bottom:20px">
				<input type="submit" name="set_filter" value="Найти" /><input type="hidden" name="set_filter" value="Y" />&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("IBLOCK_DEL_FILTER")?>" />
			</div>
	</div>
</div>

</form>

<script type="text/javascript">
	$(function () {
		$("#catalog_item_toogle_filter").click(function() {
			$("#catalog_item_filter_body").slideToggle();
		});

		
		// Обработка клика по типу изделия, показ длинны или размера
		var itemType = document.getElementsByName("arrFilter_ff[SECTION_ID]");
		$(itemType).change(function() {
			var itemLength = $('#filterLength');
			var itemSize = $('#filterSize');

			var itemSizeLeft = document.getElementsByName("arrFilter_pf[SIZE][LEFT]");
			var itemSizeRight = document.getElementsByName("arrFilter_pf[SIZE][RIGHT]");
			var itemLengthLeft = document.getElementsByName("arrFilter_pf[LENGTH][LEFT]");
			var itemLenghtRight = document.getElementsByName("arrFilter_pf[LENGTH][RIGHT]");

			$(itemSizeLeft).val('');
			$(itemSizeRight).val('');
			$(itemLenghtRight).val('');
			$(itemLengthLeft).val('');

			$(itemLength).hide();
			$(itemSize).hide();

			//Кольцо
			if($(itemType).val() == 28) {
				$(itemSize).show();
			}
			//Цепь, браслет, колье
			var arr = [31, 32, 36];
			if($.inArray(parseInt($(itemType).val()), arr) > -1) {
				$(itemLength).show();
			}
		});

	});

</script>
<?if($arParams["USE_COMPARE"]=="Y"):?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.compare.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NAME" => $arParams["COMPARE_NAME"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
	),
	$component
);?>
<br />
<?endif?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
		"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
		"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
		"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		//Template parameters
		"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
		"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
	),
	$component
);
?>
