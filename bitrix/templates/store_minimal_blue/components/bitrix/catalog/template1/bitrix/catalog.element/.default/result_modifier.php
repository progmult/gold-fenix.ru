<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//$arResult["PREVIEW_TEXT"] = strip_tags($arResult["PREVIEW_TEXT"]);
$this->__component->arResult["OFFERS_IDS"] = array();

if(is_array($arResult["OFFERS"]) && !empty($arResult["OFFERS"])){
	foreach($arResult["OFFERS"] as $arOffer){
		$this->__component->arResult["OFFERS_IDS"][] = $arOffer["ID"];
	}
}

if ($arParams['USE_COMPARE'])
{
	$delimiter = strpos($arParams['COMPARE_URL'], '?') ? '&' : '?';

	//$arResult['COMPARE_URL'] = str_replace("#ACTION_CODE#", "ADD_TO_COMPARE_LIST",$arParams['COMPARE_URL']).$delimiter."id=".$arResult['ID'];

	$arResult['COMPARE_URL'] = htmlspecialchars($APPLICATION->GetCurPageParam("action=ADD_TO_COMPARE_LIST&id=".$arResult['ID'], array("action", "id")));
}

if(is_array($arResult["DETAIL_PICTURE"]))
{
	$arFilter = '';
	if($arParams["SHARPEN"] != 0)
	{
		$arFilter = array("name" => "sharpen", "precision" => $arParams["SHARPEN"]);
	}
	$arFileTmp = CFile::ResizeImageGet(
		$arResult['DETAIL_PICTURE'],
		array("width" => $arParams["DISPLAY_DETAIL_IMG_WIDTH"], "height" => $arParams["DISPLAY_DETAIL_IMG_HEIGHT"]),
		BX_RESIZE_IMAGE_PROPORTIONAL,
		true, $arFilter
	);

	$arResult['DETAIL_PICTURE_350'] = array(
		'SRC' => $arFileTmp["src"],
		'WIDTH' => $arFileTmp["width"],
		'HEIGHT' => $arFileTmp["height"],
	);
}

if (is_array($arResult['MORE_PHOTO']) && count($arResult['MORE_PHOTO']) > 0)
{
	unset($arResult['DISPLAY_PROPERTIES']['MORE_PHOTO']);

	foreach ($arResult['MORE_PHOTO'] as $key => $arFile)
	{
		$arFilter = '';
		if($arParams["SHARPEN"] != 0)
		{
			$arFilter = array("name" => "sharpen", "precision" => $arParams["SHARPEN"]);
		}
		$arFileTmp = CFile::ResizeImageGet(
			$arFile,
			array("width" => $arParams["DISPLAY_MORE_PHOTO_WIDTH"], "height" => $arParams["DISPLAY_MORE_PHOTO_HEIGHT"]),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true, $arFilter
		);

		$arFile['PREVIEW_WIDTH'] = $arFileTmp["width"];
		$arFile['PREVIEW_HEIGHT'] = $arFileTmp["height"];

		$arFile['SRC_PREVIEW'] = $arFileTmp['src'];
		$arResult['MORE_PHOTO'][$key] = $arFile;
	}
}

if (CModule::IncludeModule('currency'))
{
	if (isset($arResult['DISPLAY_PROPERTIES']['MINIMUM_PRICE']))
		$arResult['DISPLAY_PROPERTIES']['MINIMUM_PRICE']['DISPLAY_VALUE'] = FormatCurrency($arResult['DISPLAY_PROPERTIES']['MINIMUM_PRICE']['VALUE'], CCurrency::GetBaseCurrency());
	if (isset($arResult['DISPLAY_PROPERTIES']['MAXIMUM_PRICE']))
		$arResult['DISPLAY_PROPERTIES']['MAXIMUM_PRICE']['DISPLAY_VALUE'] = FormatCurrency($arResult['DISPLAY_PROPERTIES']['MAXIMUM_PRICE']['VALUE'], CCurrency::GetBaseCurrency());
}
$this->__component->SetResultCacheKeys(array("OFFERS_IDS"));

$rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arResult['IBLOCK_ID'], "ID" => $arResult['IBLOCK_SECTION_ID']), false, array("ID", "UF_PRICE_COMMENT"));
$arSection = $rsSection->Fetch();
if(strlen($arSection['UF_PRICE_COMMENT']) > 0)
	$arResult['UF_PRICE_COMMENT'] = $arSection['UF_PRICE_COMMENT'];


$view_iblock_id = 12;

$rsView = CIBlockElement::GetList(
				array(), 
				array("IBLOCK_ID" => $view_iblock_id, 
				"=PROPERTY_ELEM_ID" => (int)$arResult['ID']),
				false,
				array('ID', 'PROPERTY_*')
			);
if($arView = $rsView->GetNextElement())
{
	$fields = $arView->GetFields();
	$properties = $arView->GetProperties();
	$arResult['TEXT_BEFORE'] = $fields['PREVIEW_TEXT'];
	$arResult['TEXT_AFTER'] = $fields['DETAIL_TEXT'];
	
	$arResult['properties'] = array();
	
	foreach ($properties as $propertie => $value) {
		$arResult['properties'][$propertie] = $value['VALUE']; 
	}
	

}
//Выборка связанных и добавление их в массив результатов
$ids_c = '';
$ids_c = $arResult['PROPERTIES']['CHILDS']['VALUE'];
$db_childs = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>3, "ID"=>$ids_c, "ACTIVE"=>"Y"),false,false,array());
while($ob = $db_childs->GetNextElement())
{
	$f = $ob->GetFields();
	$props = $ob->GetProperties();
	foreach ($props as $propertie => $value) {
		$f['PROPS'][$propertie] = $value['VALUE']; 
	}
	$price = CPrice::GetList(array(), array('PRODUCT_ID' => $f['ID'], 'CATALOG_GROUP_ID' => 1))->Fetch();
	$f['PRICE'] = $price;
	$arResult['CHILDS_ELEMENTS'][] = $f;
}
?>