<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- style is style.css -->
    <script>
        $(document).ready(function() {

            /* This is basic - uses default settings */

            $("a.fancy_images").fancybox();
            //Хочу в подарок
            $("#modal_present .modal_close_button").click(function(){
                $('#modal_present').dialog('close');
                return false;
            });
            $('#modal_present').dialog({
                autoOpen: false,
                draggable: false,
                modal: true,
                resizable: false,
                width: 600,
                //show: 'scale',
                //hide: 'scale'
            });
            $("#present_button").click(function(){
                $('#modal_present').dialog('open');
                return false;
            })
	
            $(".tabmenu li").click(function(){
                var menu_item_index = $(this).index();
                $(".tabs_content li").hide();
                $(".tabmenu li").removeClass("current");

                $(".tabs_content li").eq(menu_item_index).show();
                $(this).addClass("current");

                return false;
            });

        });
    </script>
<div itemscope itemtype="http://schema.org/Product">	
	<div class="product-card">

		<div class="images">
			<div class="image">
				<div class="picture">
					<? $renderImage = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], Array("width"=>'300','height'=>'300')); ?>
					<a rel="catalog-detail-images" class="fancy_images" href="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
						<!--<img itemprop="image" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult["NAME"]?>" id="catalog_detail_image" width="403px"/>-->
						<? echo CFile::ShowImage($renderImage['src'], $renderImage['width'], $newHeight, "border=0", "", true); ?>
					</a>
				</div>
			</div>

			<?if(count($arResult["MORE_PHOTO"])>0):
				foreach($arResult["MORE_PHOTO"] as $PHOTO):			
					?>
					<div class="miniature">
						<div class="picture"><a rel="catalog-detail-images" href="<?=$PHOTO["SRC"]?>" class="fancy_images"  title="<?=(strlen($PHOTO["DESCRIPTION"]) > 0 ? $PHOTO["DESCRIPTION"] : $arResult["NAME"])?>"><img border="0" src="<?=$PHOTO["SRC_PREVIEW"]?>" alt="<?=$arResult["NAME"]?>" /></a></div>
					</div>
				<?
				endforeach;
			endif;?>

		</div>
		<div class="text">
		<div class="product_code"><span class="art"><?=GetMessage("ART_TOVAR")?></span> <?=$arResult["PROPERTIES"]["SKU"]["VALUE"]?></div>
			<div class="name" itemprop="name"><h1><?=$arResult["NAME"]?></h1></div>
		<div class="price">
			<p>
			            <? if ($arResult["~IBLOCK_SECTION_ID"]=='117' or $arResult["~IBLOCK_SECTION_ID"]=='191'  or $arResult["~IBLOCK_SECTION_ID"]=='303' or $arResult["~IBLOCK_SECTION_ID"]=='380') : echo '2000 руб/гр';
						else :
						echo number_format(round($arResult["CATALOG_PRICE_1"]), 0, ',', ' ').' р';
						endif;
						?>
			</p>
			<?if(strlen($arResult['UF_PRICE_COMMENT']) > 0):?>
				<div><span><?=$arResult['UF_PRICE_COMMENT']?></span></div>
			<?endif;?>
			<span><?=strtolower($arResult['DISPLAY_PROPERTIES']['METAL_TYPE']['DISPLAY_VALUE'])?>, <?=strtolower($arResult['DISPLAY_PROPERTIES']['INSERT_TYPE']['DISPLAY_VALUE'])?></span>
		</div>
		  <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">		
			<meta itemprop="price" content="<?= number_format(round($arResult["CATALOG_PRICE_1"]), 2, '.', '')?>">
			<meta itemprop="priceCurrency" content="RUB">		
			<link itemprop="availability" href="http://schema.org/InStock">
		  </div>	
	<?if(intval($arResult['PROPERTIES']['ORPRICE']['VALUE']) > 0){?>
		<div class="price">
			<span style="font-style: normal; font-weight: bold;">из Вашего золота</span>
			<p class="you_price" style="    height: 50px;">
						<? if ($arResult["~IBLOCK_SECTION_ID"]=='117' or $arResult["~IBLOCK_SECTION_ID"]=='191'  or $arResult["~IBLOCK_SECTION_ID"]=='303' or $arResult["~IBLOCK_SECTION_ID"]=='380') : echo '350 руб/гр';
						else :
						echo number_format(round($arResult['PROPERTIES']['ORPRICE']['VALUE']), 0, ',', ' ').' р';
						endif;
						?>			
				<div style="text-align: center; padding-left: 60px;"><a class="btn" href="#popupform" id="open_modal" onclick="orderk('<?=$arResult["DETAIL_PAGE_URL"]?>')">ЗАКАЗАТЬ</a></div>		
			</p>			
		</div>		
	<?}?>
	<?php
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	?>
	<?if($arResult['CHILDS_ELEMENTS'][0]['ID']>0){
		$addurls = $APPLICATION->GetCurPage(false);
		?></div><?
		if($arResult['PROPERTIES']['IS_BUY']['VALUE_ENUM_ID'] != 118){
			if($arResult["CAN_BUY"]){
				//if (!in_array(8,$USER->GetUserGroupArray())){
				?>
				<style>
					.you_price{
						padding-top: 10px;
						padding-bottom: 10px;
					}

					.add_orders {
						background: url("/bitrix/templates/store_minimal_blue/images/btn/buy-btn-small.gif") no-repeat scroll 0 0 transparent;
						text-decoration: none;
						font-size: 11px;
						font-weight: bold;
						display: block;
						height: 32px;
						line-height: 32px;
						padding-left: 32px;
						text-align: left;
						color: #364450;
						font-family: sans-serif;
						text-shadow: 1px 1px #fff;
						margin: 0 auto;
					}

					.tbl_s {
						float: left;
						border: 1px solid #ccc;
						margin-bottom: 10px;
						width: 665px;
						background: none repeat scroll 0 0 #FFFFFF;
						box-shadow: 1px 1px 5px #999999;
					}

					.tbl_s1 {
						width: 83px;
						border-bottom: 1px solid #ccc;
						border-right: 1px solid #ccc;
						border-top: 1px solid #ccc;
						text-align: center;
						display: inline-block;
						line-height: 27px;
						padding: 0;
						margin: 0;
						margin-bottom: 10px;
						vertical-align: top;
						font-weight: bold;
					}

					.head_s {
						border-bottom: 1px solid #ccc;
						line-height: 28px;
						padding: 4px;
					}

					.x{
						line-height: 56px;
						height: 57px;
					}

					.head_s:last-child {
						border-bottom: none;
					}

					.tbl_s2 {
						width: 83px;
						border-right: 1px solid #ccc;
						border-bottom: 1px solid #ccc;
						border-top: 1px solid #ccc;
						text-align: center;
						display: inline-block;
						line-height: 28px;
						padding: 0;
						margin: 0;
						margin-bottom: 10px;
						margin-left: -4px;
						vertical-align: top;
					}

					.item {
						border-bottom: 1px solid #ccc;
						text-align: center;
						line-height: 28px;
						padding: 4px;
					}

					.item:last-child {
						border-bottom: none;
					}

					.tbl_s3 {
						background: url("/bitrix/templates/store_minimal_blue/images/btn/buy-btn-small_2.gif") no-repeat scroll 0 0 transparent;
						border: medium none;
						color: #364450;
						cursor: pointer !important;
						font-size: 9px;
						height: 28px;
						line-height: 20px;
						padding-left: 32px;
						text-shadow: 1px 1px #fff;
						text-transform: uppercase;
						width: 70px;
						font-weight: bold;
					}

					.tbl_s3_b {
						background: url("/bitrix/templates/store_minimal_blue/images/btn/buy-btn-small_3.gif") no-repeat scroll 0 0 transparent;
						border: medium none;
						color: #364450;
						cursor: pointer !important;
						font-size: 9px;
						height: 28px;
						line-height: 28px;
						padding-top: 0;
						text-shadow: 1px 1px #fff;
						text-transform: uppercase;
						width: 70px;
						font-weight: bold;
						display: block;
						text-decoration: none;
						margin: 0 auto;
					}

					.tbl_price {
						font-weight: bold;
					}

					.tbl_price_2 {
						/*color: #14CB32;*/
						color: #d85815;
						font-weight: bold;
					}
				</style>
				<?
					$i = 1;
				?>
				<div class="tbl_s">
					<?foreach($arResult['CHILDS_ELEMENTS'] as $child){?>
						<?if($i == 1 || $i == 8 || $i == 17){?>
							<div class="tbl_s1">
								<div class="head_s">Длина</div>
								<div class="head_s">Вес</div>
								<div class="head_s x">Цена</div>
								<div class="head_s">Из Вашего золота</div>
							</div>
						<?}?>
							<div class="tbl_s2">
								<div class="item"><?echo round($child['PROPS']['LENGTH'],1);?></div>
								<div class="item"><?echo round($child['PROPS']['WEIGHT'],2);?></div>
								<div class="item">
									<span class="tbl_price"><?echo round($child['PRICE']['PRICE'],0);?></span><br>
									<!--<input  add_url="<?echo $addurls.'?action=ADD2BASKET&id='.$child['ID'];?>"  class="add_to_cart catalog-item-buy tbl_s3" onclick="return addToCart(this, 'catalog_detail_image', 'detail', 'Добавлено', 'detail_p');" id="catalog_add2cart_link" type="button" added_text="Добавлено" value="купить"/></td>									-->
									<a class="btn" href="#popupform" id="open_modal" onclick="orderk('<?=$arResult["DETAIL_PAGE_URL"]?>')">КУПИТЬ</a>
								</div>
								<div class="item">
									<span class="tbl_price_2"><?echo round($child['PROPS']['ORPRICE'],2);?></span><br>
									<!--<a class="tbl_s3_b" href="/individual/item/?id=<?echo $child['ID'];?>">ЗАКАЗАТЬ</a>-->
									<a class="btn" href="#popupform" id="open_modal" onclick="orderk('<?=$arResult["DETAIL_PAGE_URL"]?>')">ЗАКАЗАТЬ</a>
									<!--<a class="btn" href="#popupform" id="open_modal" style="display:none" onclick="orderk('<?=$arElement["DETAIL_PAGE_URL"]?>')">ЗАКАЗАТЬ</a>-->
								</div>
							</div>
					<?$i++;}?>
				</div>
			<?}?>
			<?//}
			?>
		<?}?>
	<?
	} else {?>

		<?if($arResult['PROPERTIES']['IS_BUY']['VALUE_ENUM_ID'] != 118):?>
			<?if($arResult["CAN_BUY"]):?>
				<?php if (!in_array(8,$USER->GetUserGroupArray()) ): ?>
					<!--<input  add_url="<?=$arResult["ADD_URL"]?>"  class="add_to_cart catalog-item-buy<?/*catalog-item-in-the-cart*/?>" onclick="return addToCart(this, 'catalog_detail_image', 'detail', 'Добавлено', 'detail_p');" id="catalog_add2cart_link" type="button" added_text="Добавлено" value="купить"/>				-->
					<a class="btn" href="#popupform" id="open_modal" onclick="orderk('<?=$arResult["DETAIL_PAGE_URL"]?>')">КУПИТЬ</a>
				<?php endif; ?>
			<?endif;?>

			<br/><br/>
			<?/*<input type="button" onclick="present('<?=$arResult["NAME"]?>', 'http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>', '<?=$arUser['NAME'] .' '. $arUser['LAST_NAME']?>', '<?=$arUser['EMAIL']?>');" added_text="Добавлено" value="Хочу в подарок" class="to_present">*/?>
			<input id="present_button" type="button" added_text="Добавлено" value="Хочу в подарок" class="to_present" />			
		<?endif;?>
		</div>
	<?}?>

		<div class="clear"></div>
		<div class="information">
			<div class="block-left">
				<div class="tit">Характеристики:</div>
				<ul itemprop="description">
					<?
					if (is_array($arResult['DISPLAY_PROPERTIES']) && count($arResult['DISPLAY_PROPERTIES']) > 0):
						?>
						<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
						<? if($arProperty["DISPLAY_VALUE"] !== '0' && $arProperty["DISPLAY_VALUE"] !== 'Не выбрано'): ?>
							<li><?=$arProperty["NAME"]?>:

								<?
								if(is_array($arProperty["DISPLAY_VALUE"])):
									echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
								elseif($pid=="MANUAL"):
									?>
									<a rel="nofollow" href="<?=$arProperty["VALUE"]?>"><?=GetMessage("CATALOG_DOWNLOAD")?></a>
								<?
								else:
									echo $arProperty["DISPLAY_VALUE"];
								endif;
								?>
							</li>
						<?endif;?>
					<?endforeach;?>
					<?endif;?>
				</ul>
			</div>
			<div class="block-right">
				<div class="tit">Описание:</div>
				<?=$arResult["DETAIL_TEXT"];?>
				<?php if(strlen(trim($arResult["DETAIL_TEXT"])) == 0) echo 'Описание товара отсутствует'; ?>
			</div>
		</div>
		<div class="social-buttons">
			<?
			$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_TEMPLATE_PATH."/include/social_buttons.php"),
				false
			);
			?>
		</div>
		<!--Хочу в подарок-->

		<div style="display:none;">
			<div id="modal_present">
				<div class="modal_close_button"></div>
				<?$APPLICATION->IncludeComponent("custom:present", "present", Array(
						"USE_CAPTCHA" => "Y",	// Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей
						"OK_TEXT" => "Ваше пожелание отправлено!",	// Сообщение, выводимое пользователю после отправки
						"PRODUCT" => $arResult,
					),
					false
				);?>
			</div>
		</div>
		<div style="padding-top: 10px;">
			<?echo $arResult['TEXT_BEFORE'];?><br><?echo $arResult['TEXT_AFTER'];?>
		</div>


<?
?>



