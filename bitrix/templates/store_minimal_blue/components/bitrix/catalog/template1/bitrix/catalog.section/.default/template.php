<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$item_cnt = 0;

function aasort (&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    $c = 0;
    foreach ($sorter as $ii => $va) {
        $ret[$c]=$array[$ii];
        $c++;
    }
    $array=$ret;
}

if (isset($_GET['sort']) && $_GET['sort'] == 'price') {
    if ($_GET['order'] == 'asc') {
        aasort($arResult['ITEMS'],"CATALOG_PRICE_2");
    }
    if ($_GET['order'] == 'desc') {
        aasort($arResult['ITEMS'],"CATALOG_PRICE_2");
        $arResult['ITEMS'] = array_reverse($arResult['ITEMS']);
    }
}

$properties = $arResult['properties'];

foreach (array('title', 'description', 'keywords') as $i)
    if(!empty($properties[$i]))
        $APPLICATION->SetPageProperty($i, $properties[$i]);
?>

<?if (!empty($arResult['properties']['h1'])):?><h1><?=$arResult['properties']['h1']?></h1><?endif;?>

<?if(!empty($arResult['TEXT_BEFORE'])):?>
    <div class="section-text-after">
        <p class="MsoNormal"><?=$arResult['TEXT_BEFORE']?></p>
    </div>
<?endif;?>

<?if(!empty($arResult['ITEMS'])):?>
    <div class="block-product">
    <?
    foreach ($arResult['ITEMS'] as $key => $arElement):
			/*
    if($USER->IsAdmin()){
        print_r('<pre>');
        print_r($arElement['PROPERTIES']['ORPRICE']['VALUE']);
        print_r('</pre>');
	}*/
        $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CATALOG_ELEMENT_DELETE_CONFIRM')));

        $bHasPicture = is_array($arElement['PREVIEW_IMG']);

        $sticker = "";
        if (array_key_exists("PROPERTIES", $arElement) && is_array($arElement["PROPERTIES"]))
        {
            foreach (Array("SPECIALOFFER", "NEWPRODUCT", "SALELEADER") as $propertyCode)
                if (array_key_exists($propertyCode, $arElement["PROPERTIES"]) && intval($arElement["PROPERTIES"][$propertyCode]["PROPERTY_VALUE_ID"]) > 0)
                    $sticker .= "&nbsp;<span class=\"sticker\">".$arElement["PROPERTIES"][$propertyCode]["NAME"]."</span>";
        }

        ?>				
        <?
        // Цена
        if(count($arElement["PRICES"])>0 && $arElement['PROPERTIES']['MAXIMUM_PRICE']['VALUE'] == $arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE']):
            foreach($arElement["PRICES"] as $code=>$arPrice):
                if($arPrice["CAN_ACCESS"]):
                    ?>

                    <?								if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):
                    ?>
                    <? $price = $arPrice["PRINT_DISCOUNT_VALUE"]?>
                <?
                else:
                    ?>
                    <? $price = number_format(round($arPrice["VALUE"]), 0, ',', ' ')?>
                <?
                endif;
                    ?>
                <?
                endif;
            endforeach;
        else:
            $price_from = '';
            if($arElement['PROPERTIES']['MAXIMUM_PRICE']['VALUE'] > $arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE'])
            {
                $price = GetMessage("CR_PRICE_OT");
            }
            CModule::IncludeModule("sale")
            ?>
        <? endif; ?>

		
        <div class="product<?if($arElement['RESERVED'] == 'Y'):?> reserved-product<?endif;?>">
            <div class="img">
			<? $renderImage = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], Array("width"=>'300', "height"=>'300')); ?>
			
                <a  href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="about-element">
                    <!--<img src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>"  alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>"/>-->
					<? echo CFile::ShowImage($renderImage['src'], $newWidth, $newHeight, "border=0", "", true); ?>
                </a>
            </div>
            <a  class="name" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a>
            <?php if (!in_array(8,$USER->GetUserGroupArray()) && $arElement['RESERVED'] != 'Y' ): ?>
                <div style="width: 100%;">
                    <div class="price">					
                        <? if ($arElement["~IBLOCK_SECTION_ID"]=='117' or $arElement["~IBLOCK_SECTION_ID"]=='191'  or $arElement["~IBLOCK_SECTION_ID"]=='303' or $arElement["~IBLOCK_SECTION_ID"]=='380') : echo '2000 руб/гр'; 
						else :
						echo number_format(round($arElement["CATALOG_PRICE_1"]), 0, ',', ' ').' р';
						endif;
						?>
                    </div>
                    <div class="buy">
                        <!--<input  add_url="<?echo $arElement["ADD_URL"]?>"  class="buy-btn catalog-item-buy"
                                onclick="return addToCart(this, 'catalog_list_image_<?=$arElement['ID']?>', 'list', 'Добавлено', 'catalog_add2cart_link_<?=$arElement['ID']?>');"
                                id="first_catalog_add2cart_link_<?=$arElement['ID']?>" type="button" added_text="Добавлено" value="купить"/>-->
						<a class="tbl_s3_b" href="#popupform" id="popupbutton" onclick="orderk('<?=$arElement["DETAIL_PAGE_URL"]?>')">КУПИТЬ</a>
                    </div>
                </div>
<?
				if(intval($arElement['PROPERTIES']['ORPRICE']['VALUE']) > 0){
?>
                <div style="width: 100%;">
					<div style="font-size: 12px; font-weight: bold; width: 100%; padding-top: 3px; padding-bottom: 3px;">из Вашего золота</div>
                    <div class="price">
					    <? if ($arElement["~IBLOCK_SECTION_ID"]=='117' or $arElement["~IBLOCK_SECTION_ID"]=='191'  or $arElement["~IBLOCK_SECTION_ID"]=='303' or $arElement["~IBLOCK_SECTION_ID"]=='380') : echo '350 руб/гр';
						else :
						echo number_format(round($arElement['PROPERTIES']['ORPRICE']['VALUE']), 0, ',', ' ').' р';
						endif;
						?>
                    </div>
                    <div class="buy">
						<a class="tbl_s3_b" href="#popupform" id="popupbutton" onclick="orderk('<?=$arElement["DETAIL_PAGE_URL"]?>')">ЗАКАЗАТЬ</a>
                    </div>
                </div>
<?
}

?>


            <?php endif; ?>
        </div>
        <?php if ($item_cnt == 3) {echo '</tr><tr>'; $item_cnt = -1;} $item_cnt ++;?>
    <?endforeach;?>

    <div class="clear"></div>

    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"];?>
    <?endif;?>
<?endif;?>

<?if(!empty($arResult['TEXT_AFTER'])):?>
    <div class="section-text-after">
        <p class="MsoNormal"><?=$arResult['TEXT_AFTER']?></p>
    </div>
<?endif;?>