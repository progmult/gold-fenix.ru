<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string

__IncludeLang(dirname(__FILE__).'/lang/'.LANGUAGE_ID.'/'.basename(__FILE__));

$curPage = $GLOBALS['APPLICATION']->GetCurPage($get_index_page=false);

if ($curPage != SITE_DIR)
{
	if (empty($arResult) || $curPage != urldecode($arResult[count($arResult)-1]['LINK']))
		$arResult[] = array('TITLE' =>  htmlspecialcharsback($GLOBALS['APPLICATION']->GetTitle(false, true)), 'LINK' => $curPage);
}

if(empty($arResult))
	return "";
	
$strReturn = '<div class="breadcrumbs">';
if($_SERVER['PHP_SELF'] != '/index.php')
{
	for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
	{
		$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
		
		if(($arResult[$index]["LINK"] <> "" && $itemSize-1 !== $index) || ($title == 'Каталог' && isset($_GET['arrFilter_pf']['BRAND']))) {
			$strReturn .= '<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a> ';
			if (($title == 'Каталог' && isset($_GET['arrFilter_pf']['BRAND']))) {} else $strReturn .= '» ';
		}
		else
			$strReturn .= '<span>'.$title.'</span>';
	}


	if (isset($_GET['arrFilter_pf']['BRAND'])) $strReturn .= ' » <span>БРЕНДОВЫЕ ИЗДЕЛИЯ</span>';
}

$strReturn .= '</div>';

return $strReturn;
?>