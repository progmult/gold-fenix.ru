<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<i id="cart-status">
<?
// Подсчет окончания товаров
$cart_ending = '';
if ($arResult["NUM_PRODUCTS"] > 1 && $arResult["NUM_PRODUCTS"] < 5) $cart_ending = 'а';
if ($arResult["NUM_PRODUCTS"] > 4) $cart_ending = 'ов';


if (IntVal($arResult["NUM_PRODUCTS"])>0)
{
	if(CModule::IncludeModule("sale"))
	{
	// Подсчет стоимости товаров
	$arBasket = GetBasketList();
	$arResult["TOTALPRICE"] = 0;
	foreach($arBasket as $arBasketItem) {
		$arResult["TOTALPRICE"] += $arBasketItem["PRICE"] * $arBasketItem["QUANTITY"];
	}
}
?>
<a rel="nofollow" href="<?=$arParams["PATH_TO_BASKET"]?>"><?=$arResult["NUM_PRODUCTS"]?> товар<?=$cart_ending?> на <?=round($arResult["TOTALPRICE"])?> руб</a>
<?
}
else
{
?>
	<a rel="nofollow" href="<?=$arParams["PATH_TO_BASKET"]?>"><?echo GetMessage('YOUR_CART_EMPTY')?></a>
<?
}
?>
</i>