<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
function PrintPropsForm($arSource=Array(), $arResultGlobal)
{
	if (!empty($arSource))
	{
		foreach($arSource as $arProperties)
		{
			if ($arProperties["TYPE"] == "LOCATION" && count($arProperties["VARIANTS"]) == 1)
			{
				$arC = array_values($arProperties["VARIANTS"]);
				?>
				<tr style="display:none;"><td colspan="2" style="display:none;">
				<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="<?=$arC[0]["ID"]?>">
				</td></tr>
				<?
			}
			else
			{
				//Поле Предпочтительное время доставки
				$bCourier = false;
				if( $arProperties['CODE'] === 'DELIVERY_HOUR')
				{
					if(!empty($arResultGlobal['DELIVERY']))
					{
						foreach($arResultGlobal['DELIVERY'] as $deliv)
						{
							if($deliv['ID'] == 1 && $deliv['CHECKED'] == 'Y')
								$bCourier = true;
						}
					}
				}
				if($arProperties['CODE'] === 'DELIVERY_HOUR' && !$bCourier)
				{
					continue;
				}
				//Поле пункт самовывоза
				$bSelf = false;
				if( $arProperties['CODE'] === 'office')
				{
					if(!empty($arResultGlobal['DELIVERY']))
					{
						foreach($arResultGlobal['DELIVERY'] as $deliv)
						{
							if($deliv['ID'] == 2 && $deliv['CHECKED'] == 'Y')
								$bSelf = true;
						}
					}
				}
				if($arProperties['CODE'] === 'office' && !$bSelf)
				{
					continue;
				}
				?>
				<tr>
					<td valign="top" align="right">
						<?if($arProperties["REQUIED_FORMATED"]=="Y")
						{
							?><span class="starrequired">*</span><?
						}
						echo $arProperties["NAME"] ?>:
					</td>
					<td>
						<?
						if($arProperties["TYPE"] == "CHECKBOX")
						{
							?>
							
							<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="">
							<input type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="Y"<?if ($arProperties["CHECKED"]=="Y") echo " checked";?>>
							<?
						}
						elseif($arProperties["TYPE"] == "TEXT")
						{
							?>
							<? if( $arProperties['CODE'] === 'UF_METRO'): ?>
							<?php
								$u = CUser::GetById($GLOBALS['USER']->GetId());
								$u = $u->Fetch();
								$GLOBALS['APPLICATION']->IncludeComponent("custom:metro", "", array(
									'iblock_id' => 9,
									'field_name' => $arProperties["FIELD_NAME"],
									'val' => $u["UF_METRO"],
								));
							?> 
							<? elseif( $arProperties['CODE'] === 'PHONE'): ?>
							<?php
								if(empty($arProperties["VALUE"]))
								{
									$u = CUser::GetById($GLOBALS['USER']->GetId());
									$u = $u->Fetch();
								}
								else
									$u["PERSONAL_PHONE"] = $arProperties["VALUE"];
							?>
							<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maskedinput-1.2.2.js"></script>
							<script>
								jQuery(function($){
									$.mask.definitions['~']='[+-]';
									$('#<?=$arProperties["FIELD_NAME"]?>').mask('+7(999)999-99-99').addClass('masked_field');
									$('#<?=$arProperties["FIELD_NAME"]?>').live('focus', function(){
										if(!$(this).hasClass('masked_field'))
										{
											$('#<?=$arProperties["FIELD_NAME"]?>').mask('+7(999)999-99-99').addClass('masked_field');
										}
									});
								});
							</script>
							<input type="text" maxlength="250" size="<?=$arProperties["SIZE1"]?>" value="<?=$u["PERSONAL_PHONE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" style="width:300px;"> 
							<? else: ?>
							<input type="text" maxlength="250" size="<?=$arProperties["SIZE1"]?>" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" style="width:300px;">
							<? endif ?>
							<?
						}
						elseif($arProperties["TYPE"] == "SELECT")
						{
							?>
							<select name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>" style="width:300px;">
							<?
							foreach($arProperties["VARIANTS"] as $arVariants)
							{
								?>
								<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
								<?
							}
							?>
							</select>
							<?
						}
						elseif ($arProperties["TYPE"] == "MULTISELECT")
						{
							?>
							<select multiple name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>" style="width:300px;">
							<?
							foreach($arProperties["VARIANTS"] as $arVariants)
							{
								?>
								<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
								<?
							}
							?>
							</select>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXTAREA")
						{
							?>
							<textarea rows="<?=$arProperties["SIZE2"]?>" cols="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" style="width:300px;"><?=$arProperties["VALUE"]?></textarea>
							<?
						}
						elseif ($arProperties["TYPE"] == "LOCATION")
						{
							
							$arParams["COUNTRY"] = 5;
							$arParams['ALLOW_EMPTY_CITY'] = 'Y';
							$arParams["CITY_OUT_LOCATION"] = 'Y';
							$arRes["CITY_LIST"] = array();
							if ($arParams["COUNTRY"] > 0)
							{
								$rsLocationsList = CSaleLocation::GetList(
									array(
										"SORT" => "ASC",
										"COUNTRY_NAME_LANG" => "ASC",
										"CITY_NAME_LANG" => "ASC"
									),
									array(
										"COUNTRY_ID" => $arParams["COUNTRY"],
										"LID" => LANGUAGE_ID,
									),
									false,
									false,
									array(
										"ID", "CITY_ID", "CITY_NAME"
									)
								);
								
								while ($arCity = $rsLocationsList->GetNext())
								{
									if ($arParams['ALLOW_EMPTY_CITY'] == 'Y' || $arCity['CITY_ID'] > 0)
									{
										$arRes["CITY_LIST"][] = array(
											"ID" => $arCity[$arParams["CITY_OUT_LOCATION"] == "Y" ? "ID" : "CITY_ID"],
											"CITY_ID" => $arCity['CITY_ID'],
											"CITY_NAME" => $arCity["CITY_NAME"],
										);
									}
								}
							}
							
							$value = 0;
							foreach ($arProperties["VARIANTS"] as $arVariant) 
							{
								if ($arVariant["SELECTED"] == "Y") 
								{
									$value = $arVariant["ID"]; 
									break;
								}
							}
							?>
							<input type="hidden" name="COUNTRY_ORDER_PROP_5" value="5" />
							<select name="ORDER_PROP_5" onchange="submitForm()">
								<option><?echo GetMessage('SAL_CHOOSE_CITY')?></option>
								<?foreach ($arRes["CITY_LIST"] as $arCity):?>
									<option value="<?=$arCity["ID"]?>"<?if ($arCity["ID"] == $value):?> selected="selected"<?endif;?>><?=($arCity['CITY_ID'] > 0 ? $arCity["CITY_NAME"] : GetMessage('SAL_CHOOSE_CITY_OTHER'))?></option>
								<?endforeach;?>
							</select>
							<?
							/*
							$GLOBALS["APPLICATION"]->IncludeComponent(
								'bitrix:sale.ajax.locations', 
								'', 
								array(
									"AJAX_CALL" => "N", 
									"COUNTRY_INPUT_NAME" => "COUNTRY_".$arProperties["FIELD_NAME"],
									"CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
									"CITY_OUT_LOCATION" => "Y",
									"LOCATION_VALUE" => $value,
									"ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
								),
								null,
								array('HIDE_ICONS' => 'Y')
							);
							*/
						}
						elseif ($arProperties["TYPE"] == "RADIO")
						{
							foreach($arProperties["VARIANTS"] as $arVariants)
							{
								?>
								<input type="radio" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>" value="<?=$arVariants["VALUE"]?>"<?if($arVariants["CHECKED"] == "Y") echo " checked";?>> <label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"><?=$arVariants["NAME"]?></label><br />
								<?
							}
						}

						if (strlen($arProperties["DESCRIPTION"]) > 0)
						{
							?><br /><small><?echo $arProperties["DESCRIPTION"] ?></small><?
						}
						?>
						
					</td>
				</tr>
				<?
			}
		}

		return true;
	}
	return false;
}
?>
<div class="order-item">
<div class="order-title">
	<b class="r2"></b><b class="r1"></b><b class="r0"></b>
	<div class="order-title-inner">
		<span><?=GetMessage("SOA_TEMPL_PROP_INFO")?></span>
	</div>
</div>
<div class="order-info">
	<div style="display:none;">
	<?
		$APPLICATION->IncludeComponent(
			'bitrix:sale.ajax.locations', 
			'', 
			array(
				"AJAX_CALL" => "N", 
				"COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
				"CITY_INPUT_NAME" => "tmp",
				"CITY_OUT_LOCATION" => "Y",
				"LOCATION_VALUE" => "",
				"ONCITYCHANGE" => "",
			),
			null,
			array('HIDE_ICONS' => 'Y')
		);
	?>
	</div>
	<table cellspacing="0" cellpadding="6">
	<tbody>
		<?
		if(!empty($arResult["ORDER_PROP"]["USER_PROFILES"]))
		{
			?>
			<tr>
				<td valign="top" align="right">
					<?=GetMessage("SOA_TEMPL_PROP_PROFILE")?>
				</td>
				<td>
					
					<script language="JavaScript">
					function SetContact(profileId)
					{
						document.getElementById("profile_change").value = "Y";
						submitForm();
					}
					</script>
					<input type="hidden" name="profile_change" id="profile_change" value="N">
					<select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
						<option value="0"><?=GetMessage("SOA_TEMPL_PROP_NEW_PROFILE")?></option>
						<?
						foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
						{
							?>
							<option value="<?= $arUserProfiles["ID"] ?>"<?if ($arUserProfiles["CHECKED"]=="Y") echo " selected";?>><?=$arUserProfiles["NAME"]?></option>
							<?
						}
						?>
					</select><br />
					<?=GetMessage("SOA_TEMPL_PROP_CHOOSE_DESCR")?>
				</td>
			</tr>
			<?
		}
		PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arResult);
		PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arResult);
		?>
	</tbody>
	</table>
</div>
</div>
<?

//echo '<pre>'; print_r($arTest); echo '</pre>';
?>