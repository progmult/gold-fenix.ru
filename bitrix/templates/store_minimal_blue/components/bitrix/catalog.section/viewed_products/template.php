<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$countItems = count($arResult['ITEMS']);

if ($countItems < 1) {
	echo 'По Вашему запросу товары не найдены';
	return;
	
}

$item_cnt = 0;

function aasort (&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    $c = 0;
    foreach ($sorter as $ii => $va) {
        $ret[$c]=$array[$ii];
        $c++;
    }
    $array=$ret;
}

if (isset($_GET['sort']) && $_GET['sort'] == 'price') {
	if ($_GET['order'] == 'asc') {
		aasort($arResult['ITEMS'],"CATALOG_PRICE_2");
	}
	if ($_GET['order'] == 'desc') {
		aasort($arResult['ITEMS'],"CATALOG_PRICE_2");
		$arResult['ITEMS'] = array_reverse($arResult['ITEMS']);
	}
}

?> 

<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jcarousel/jcarousellite_1.0.1.min.js');?>

<p class="socials-title">Вы смотрели ранее</p>
<div class="catalog-detail-line"></div>
<div class="block-product" style="padding:15px;">
	<?if($countItems > 3):?>
	<script>
		jQuery(function($){
			$("#products_slider_1").jCarouselLite({
				btnNext: "#right_arrow",
				btnPrev: "#left_arrow",
				scroll:1,
				circular:true
			});
		});
	</script>
	<a rel="nofollow" class="products_slider_arrows" id="right_arrow">Вправо</a>
	<a rel="nofollow" class="products_slider_arrows" id="left_arrow">Влево</a>
	<?endif;?>
	<div id="products_slider_1" style="overflow:hidden; width:650px; height:275px;">
		<?if($countItems > 3):?>
		<ul>
		<?endif;?>
<?
$j = 0;
$count_elements = 6;
$dsss=0;
foreach ($arResult['ITEMS'] as $key => $arElement):
$dsss=$dsss+1;
if ($dsss<6) {
	$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CATALOG_ELEMENT_DELETE_CONFIRM')));

	$bHasPicture = is_array($arElement['PREVIEW_IMG']);

	$sticker = "";
	if (array_key_exists("PROPERTIES", $arElement) && is_array($arElement["PROPERTIES"]))
	{
		foreach (Array("SPECIALOFFER", "NEWPRODUCT", "SALELEADER") as $propertyCode)
			if (array_key_exists($propertyCode, $arElement["PROPERTIES"]) && intval($arElement["PROPERTIES"][$propertyCode]["PROPERTY_VALUE_ID"]) > 0)
				$sticker .= "&nbsp;<span class=\"sticker\">".$arElement["PROPERTIES"][$propertyCode]["NAME"]."</span>";
	}

?>
<?
// Цена
			if(count($arElement["PRICES"])>0 && $arElement['PROPERTIES']['MAXIMUM_PRICE']['VALUE'] == $arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE']):
				foreach($arElement["PRICES"] as $code=>$arPrice):
					if($arPrice["CAN_ACCESS"]):
?>

<?								if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):
?>
									<? $price = $arPrice["PRINT_DISCOUNT_VALUE"]?> 
<?
								else:
?>
									<? $price = number_format(round($arPrice["VALUE"]), 0, ',', ' ')?>
<?
								endif;
?>	
                        <? if ($arElement["~IBLOCK_SECTION_ID"]=='117' or $arElement["~IBLOCK_SECTION_ID"]=='191'  or $arElement["~IBLOCK_SECTION_ID"]=='303' or $arElement["~IBLOCK_SECTION_ID"]=='380') : $price='2000 руб/г';
						else :
						$price = number_format(round($arPrice["VALUE"]), 0, ',', ' ');
						endif;
						?>					
<?
					endif;
				endforeach;
			else:
				$price_from = '';
				if($arElement['PROPERTIES']['MAXIMUM_PRICE']['VALUE'] > $arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE'])
				{
					 $price = GetMessage("CR_PRICE_OT");	
				}
				CModule::IncludeModule("sale")
?>
<? endif; ?>
	<?if($countItems > 3):?>
	<li>
	<?endif;?>
		<div class="viewed_product">
			<div class="viewed_product_img">
			<? $renderImage = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], Array("width"=>'150', "height"=>'150')); ?>	
				<a rel="nofollow" href="<?=$arElement["DETAIL_PAGE_URL"]?>">
				<? echo CFile::ShowImage($renderImage['src'], $newWidth, $newHeight, "border=0", "", true); ?>
				</a>
			</div>
			
			<div class="viewed_product_price">
				<?=$price?>р
			</div>
			<div class="viewed_product_name">
				<?=$arElement["NAME"]?>
			</div>
			<?php if (!in_array(8,$USER->GetUserGroupArray()) ): ?>
			<div class="buy">
				<!--<input add_url="<?echo $arElement["ADD_URL"]?>"  class="buy-btn catalog-item-buy<?/*catalog-item-in-the-cart*/?>" onclick="return addToCart(this, 'catalog_list_image_<?=$arElement['ID']?>', 'list', 'Добавлено', 'catalog_add2cart_link_<?=$arElement['ID']?>');" id="first_catalog_add2cart_link_<?=$arElement['ID']?>" type="button" added_text="Добавлено" value="купить"/>-->
				<a class="btn" href="#popupform" id="open_modal" onclick="orderk('<?=$arElement["DETAIL_PAGE_URL"]?>')">КУПИТЬ</a>
			</div>
			<?php endif; ?>	
		</div>
	<?if($countItems > 3):?>
	</li>
	<?endif;?>
<?} endforeach;?>
<?if($countItems > 3):?>
</ul>
<?endif;?>
</div>
</div>
<div class="clear"></div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"];?>
<?endif;?>	