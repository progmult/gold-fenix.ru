<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
	jQuery(function($){
		/*
		//Обработчик оформления заказа для незарег.пользователя
		$("#basketOrderButton2").on('click', function(){
			var is_auth = <?=(int)$USER->IsAuthorized()?>;
			if(is_auth <= 0)
			{
				$("#login_form input[name=backurl]").val('/personal/order/make/');
				$("a#go_login").click();
				return false;
			}
		});
		*/
	});
</script>

<?/*<h2>До окончания резервации товара осталось <b><?=date('i:s', ($_SESSION['cart_reserved_time']) - time());?></b></h2>*/?>
<p>Каждый товар, положеный Вами в корзину, резервируется на <b><?=RESERVE_TIME?> мин.</b> Если в течение этого времени не будет оформлен заказ - товар автоматически убирается из Вашей корзины и становится доступен остальным покупателям</p>

<div class="cart-items" id="id-cart-list">
	<div class="inline-filter cart-filter" style="display:none">
		<label><?=GetMessage("SALE_PRD_IN_BASKET")?></label>&nbsp;
			<b><?=GetMessage("SALE_PRD_IN_BASKET_ACT")?></b>&nbsp;
			<a rel="nofollow" href="#" onclick="ShowBasketItems(2);"><?=GetMessage("SALE_PRD_IN_BASKET_SHELVE")?> (<?=count($arResult["ITEMS"]["DelDelCanBuy"])?>)</a>&nbsp;
			<?if(false):?>
			<a rel="nofollow" href="#" onclick="ShowBasketItems(3);"><?=GetMessage("SALE_PRD_IN_BASKET_NOTA")?> (<?=count($arResult["ITEMS"]["nAnCanBuy"])?>)</a>
			<?endif;?>
	</div>

	<?if(count($arResult["ITEMS"]["AnDelCanBuy"]) > 0):?>
	<table class="cart-items" cellspacing="0">
	<thead>
		<tr>
			<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-name"><?= GetMessage("SALE_NAME")?></td> 
			<?endif;?>
			<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-price"><?= GetMessage("SALE_PRICE")?></td>
			<?endif;?>
			<?if (in_array("VAT", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-price"><?= GetMessage("SALE_VAT")?></td>
			<?endif;?>
			<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-type"><?= GetMessage("SALE_PRICE_TYPE")?></td>
			<?endif;?>
			<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-discount"><?= GetMessage("SALE_DISCOUNT")?></td>
			<?endif;?>
			<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-weight"><?= GetMessage("SALE_WEIGHT")?></td>
			<?endif;?>
			<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-quantity"><?= GetMessage("SALE_QUANTITY")?></td>
			<?endif;?>
			<td>Дополнительно</td>
			<td class="cart-item-actions">
				<?if (in_array("DELETE", $arParams["COLUMNS_LIST"]) || in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
					<?= GetMessage("SALE_ACTION")?>
				<?endif;?>
			</td>
		</tr>
	</thead>
	<tbody>
	<?
	// print '<pre>'; print_R ($arResult["ITEMS"]["AnDelCanBuy"]); print '</pre>';
	$i=0;
	foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems)
	{	
		$url_pos = strpos($arBasketItems["DETAIL_PAGE_URL"],'_child_');
		if($url_pos > 0){$arBasketItems["DETAIL_PAGE_URL"] = substr($arBasketItems["DETAIL_PAGE_URL"],0,$url_pos).'/';}
		if (strpos($arBasketItems['DETAIL_PAGE_URL'], 'packaging') === false && strpos($arBasketItems['DETAIL_PAGE_URL'], 'extra') === false) {
		?>
		<tr>
			<?php
			$arSelect = Array("*");
			$arFilter = Array("IBLOCK_ID"=>3, "ID"=>$arBasketItems["PRODUCT_ID"]);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
			while($ob = $res->GetNextElement())
			{
			  $arResult_item = $ob->GetFields();
			  $arFile = CFile::GetFileArray($arResult_item["PREVIEW_PICTURE"]);
			  	$props = $ob->GetProperties();
				foreach ($props as $propertie => $value) {
					$f['PROPS'][$propertie] = $value['VALUE']; 
				}
			}

			?>

			<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-name"><?
				if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):
					?><a rel="nofollow" href="<?=$arBasketItems["DETAIL_PAGE_URL"] ?>"><?
				endif;
					?><b><?echo $arBasketItems["NAME"] ?></b><?if(strlen($f['PROPS']['WEIGHT']) > 0){echo ' ('.round($f['PROPS']['WEIGHT'],2).' г.)';}?><?
				if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):
					?></a><?
				endif;?>
				<?if (in_array("PROPS", $arParams["COLUMNS_LIST"]))
				{
					foreach($arBasketItems["PROPS"] as $val)
					{
						echo "<br />".$val["NAME"].": ".$val["VALUE"];
					}
				}?><a rel="nofollow" href="<?=$arBasketItems["DETAIL_PAGE_URL"] ?>"><img style="border:0px;float:right;width:80px" src="<?=$arFile["SRC"]?>"></a>
				</td>
			<?endif;?>
			<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-price"><?=$arBasketItems["PRICE_FORMATED"]?></td>
			<?endif;?>
			<?if (in_array("VAT", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-price"><?=$arBasketItems["VAT_RATE_FORMATED"]?></td>
			<?endif;?>
			<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-type"><?=$arBasketItems["NOTES"]?></td>
			<?endif;?>
			<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-discount"><?=$arBasketItems["DISCOUNT_PRICE_PERCENT_FORMATED"]?></td>
			<?endif;?>
			<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-weight"><?=$arBasketItems["WEIGHT_FORMATED"]?></td>
			<?endif;?>
			<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-quantity"><input maxlength="18" type="text" name="QUANTITY_<?=$arBasketItems["ID"] ?>" value="<?=$arBasketItems["QUANTITY"]?>" size="3"></td>
			<?endif;?>
			<td>
				<?/*
				<select class="extra_options" style="width:150px;" name="packaging-<?=$arBasketItems["ID"]?>" goodsid="<?=$arBasketItems["ID"]?>"  goodspid="<?=$arBasketItems["PRODUCT_ID"]?>" gtype="packaging">
					<option value="">Без упаковки</option>

					<?php
					if(CModule::IncludeModule("iblock") && CModule::IncludeModule("sale"))
					{
						// Получаю обертку к товару
						$selected = 0;
					    $arBasketItems_packaging = array();

					    $dbBasketItems_packaging = CSaleBasket::GetList(
					        array(),
					        array(
					             "FUSER_ID" => CSaleBasket::GetBasketUserID(),
					           	 "LID" => SITE_ID,
					           	 "DETAIL_PAGE_URL" => 'packaging/'.$arBasketItems["PRODUCT_ID"],
					             "ORDER_ID" => "NULL"
					        ),
					        false,
					        false,
					        array("ID", "DETAIL_PAGE_URL", "PRODUCT_ID")
					    );

					    $i = 0;
					    while ($arItems_packaging = $dbBasketItems_packaging->Fetch())
					    {
					    	$selected = $arItems_packaging['PRODUCT_ID'];
					    }

						$arSelect = Array("ID", "NAME", "CATALOG_GROUP_2");
						$arFilter = Array("IBLOCK_ID"=>6, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
						$res = CIBlockElement::GetList(Array("CATALOG_PRICE", "ASC"), $arFilter, false, Array("nPageSize"=>999), $arSelect);
						while($ob = $res->GetNextElement())
						{
							$arFields = $ob->GetFields();
							if ($selected == $arFields['ID']) $selected_option = ' selected="selected"'; else $selected_option = '';
							echo '<option '.$selected_option.' value="'.$arFields['ID'].'">'.$arFields['NAME'].' &ndash; '.$arFields['CATALOG_PRICE_2'].' руб.</option>';
						}
					}
					?> 

				</select><br>
<!--
				<select  class="extra_options" style="width:150px;" name="extra-<?=$arBasketItems["ID"]?>" goodsid="<?=$arBasketItems["ID"]?>"  goodspid="<?=$arBasketItems["PRODUCT_ID"]?>" gtype="extra">
					<option value='0'>Без обработки</option>

					<?php
					if(CModule::IncludeModule("iblock") && CModule::IncludeModule("sale"))
					{
						// Получаю обертку к товару
						$selected = 0;
					    $arBasketItems_packaging = array();

					    $dbBasketItems_packaging = CSaleBasket::GetList(
					        array(),
					        array(
					             "FUSER_ID" => CSaleBasket::GetBasketUserID(),
					           	 "LID" => SITE_ID,
					           	 "DETAIL_PAGE_URL" => 'extra/'.$arBasketItems["PRODUCT_ID"],
					             "ORDER_ID" => "NULL"
					        ),
					        false,
					        false,
					        array("ID", "DETAIL_PAGE_URL", "PRODUCT_ID")
					    );

					    $i = 0;
					    while ($arItems_packaging = $dbBasketItems_packaging->Fetch())
					    {
					    	$selected = $arItems_packaging['PRODUCT_ID'];
					    }

						$arSelect = Array("ID", "NAME", "CATALOG_GROUP_2");
						$arFilter = Array("IBLOCK_ID"=>8, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
						$res = CIBlockElement::GetList(Array("CATALOG_PRICE", "ASC"), $arFilter, false, Array("nPageSize"=>999), $arSelect);
						while($ob = $res->GetNextElement())
						{
							$arFields = $ob->GetFields();
							if ($selected == $arFields['ID']) $selected_option = ' selected="selected"'; else $selected_option = '';
							echo '<option '.$selected_option.' value="'.$arFields['ID'].'">'.$arFields['NAME'].' &ndash; '.$arFields['CATALOG_PRICE_2'].' руб.</option>';
						}
					}
					?> 
				</select>
-->				
			*/?>
			</td>
			<td class="cart-item-actions">
				<?if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
					<a rel="nofollow" class="cart-delete-item" href="<?=str_replace("#ID#", $arBasketItems["ID"], $arUrlTempl["delete"])?>" title="<?=GetMessage("SALE_DELETE_PRD")?>">Удалить</a>
				<?endif;?>
				<?if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
					<a rel="nofollow" class="cart-shelve-item" href="<?=str_replace("#ID#", $arBasketItems["ID"], $arUrlTempl["shelve"])?>"><?=GetMessage("SALE_OTLOG")?></a>
				<?endif;?>
			</td>
		</tr>
		<?
	}
		$i++;
	}
	?>
	</tbody>
	<tfoot>
		<tr>
			<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-name">
					<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
						<p><?echo GetMessage("SALE_ALL_WEIGHT")?>:</p>
					<?endif;?>
					<?if (doubleval($arResult["DISCOUNT_PRICE"]) > 0)
					{
						?><p><?echo GetMessage("SALE_CONTENT_DISCOUNT")?><?
						if (strLen($arResult["DISCOUNT_PERCENT_FORMATED"])>0)
							echo " (".$arResult["DISCOUNT_PERCENT_FORMATED"].")";?>:</p><?
					}?>
					<?if ($arParams['PRICE_VAT_SHOW_VALUE'] == 'Y'):?>
						<p><?echo GetMessage('SALE_VAT_EXCLUDED')?></p>
						<p><?echo GetMessage('SALE_VAT_INCLUDED')?></p>
					<?endif;?>
					<p><b><?= GetMessage("SALE_ITOGO")?>:</b></p>
				</td>
			<?endif;?>
			<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-price">
					<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
						<p><?=$arResult["allWeight_FORMATED"]?></p>
					<?endif;?>
					<?if (doubleval($arResult["DISCOUNT_PRICE"]) > 0):?>
						<p><?=$arResult["DISCOUNT_PRICE_FORMATED"]?></p>
					<?endif;?>
					<?if ($arParams['PRICE_VAT_SHOW_VALUE'] == 'Y'):?>
						<p><?=$arResult["allNOVATSum_FORMATED"]?></p>
						<p><?=$arResult["allVATSum_FORMATED"]?></p>
					<?endif;?>
					<p><b><?=$arResult["allSum_FORMATED"]?></b></p>
				</td>
			<?endif;?>
			<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-type">&nbsp;</td>
			<?endif;?>
			<?if (in_array("VAT", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-type">&nbsp;</td>
			<?endif;?>
			<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-discount">&nbsp;</td>
			<?endif;?>
			<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-weight">&nbsp;</td>
			<?endif;?>
			<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-quantity">&nbsp;</td>
			<?endif;?>
			<?if (in_array("DELETE", $arParams["COLUMNS_LIST"]) || in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
				<td class="cart-item-actions">&nbsp;</td>
			<?endif;?>
		</tr>
	</tfoot>
	</table>

	<div class="cart-ordering">
		<?if ($arParams["HIDE_COUPON"] != "Y"):?>
			<div class="cart-code">
				<input <?if(empty($arResult["COUPON"])):?>onclick="if (this.value=='<?=GetMessage("SALE_COUPON_VAL")?>')this.value=''" onblur="if (this.value=='')this.value='<?=GetMessage("SALE_COUPON_VAL")?>'"<?endif;?> value="<?if(!empty($arResult["COUPON"])):?><?=$arResult["COUPON"]?><?else:?><?=GetMessage("SALE_COUPON_VAL")?><?endif;?>" name="COUPON">
			</div>
		<?endif;?>
		<div class="cart-buttons">
			<input type="submit" value="<?echo GetMessage("SALE_UPDATE")?>" name="BasketRefresh">
			<input type="submit" value="<?echo GetMessage("SALE_ORDER")?>" name="BasketOrder"  id="basketOrderButton2">
		</div>
	</div>
	<?else:
		echo ShowNote(GetMessage("SALE_NO_ACTIVE_PRD"));
	endif;?>
</div>

<script>
$(".extra_options").change(function() {
	$.get("/include/ajax/cart.php", { action: "update_extras", id: $(this).attr('goodsid'), pid: $(this).attr('goodspid'), value: $(this).val(), gtype: $(this).attr('gtype') },
		function(data){
			window.location = '/personal/cart';
		});
});
</script>

<?