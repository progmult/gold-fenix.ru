<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="header-search" style="width:auto!important;right:0px!important;">
	<form action="<?=$arResult["FORM_ACTION"]?>">
		<input type="submit" id="search_submit" name="s" value="Найти" />
		<input type="text" style="width: 60% !important;" name="q" placeholder="<?/*<?if((int)$_SESSION['SEARCH_MODE'] > 0):?>Поиск по каталогу<?else:?>Поиск по сайту<?endif;?>*/?>Поиск по каталогу" id="qg" value="" />
	</form>
	<a rel="nofollow" id="clear-button" title="Очистить строку поиска"><img src="<?=SITE_TEMPLATE_PATH?>/images/clear-icon.png" /></a>
	<?/*
	<!--Переключатель поиска-->
	<script>
		$(function() {
			$( "#search_toggle" ).button();
			$( "#search_toggle" ).on('click', function(){
				if($(this).val() == 0)
					$(this).val(1);
				else
					$(this).val(0);
				var value = $(this).val();
		
				$.ajaxSetup({
					async:false
				});
				
				$.post(
					'/bitrix/js/ajax.php',
					{
						action: 'changeSearchMode',
						mode: value
					},
					function(data)
					{
						location.replace('<?='http://'.$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri()?>');
					}
				);
				
			});
		});
	</script>
	<div id="search_switch_mode">
		<input type="checkbox" id="search_toggle" value="<?=(int)$_SESSION['SEARCH_MODE']?>"<?if((int)$_SESSION['SEARCH_MODE'] > 0):?> checked="checked"<?endif;?> /><label for="search_toggle">Поиск только в каталоге</label>
	</div>
	<!---->
	*/?>
</div>
<script>
	jQuery(function(){
		$( "#qg" ).autocomplete({
		  source: "/bitrix/js/search.php",
		  minLength: 2,
		  select: function( event, ui ) {
				$( "#qg" ).val(ui.item.value);
				$("#search_submit").click();
			//window.location.href = ui.item.link;
			//$( "#qg" ).val(ui.item.label)
		  }
		});
	});
</script>