<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
function checkPrice(el) {
	var n = $(el).val();
	if (isNaN(n)) alert ('Введите число');
}
</script>
<?//echo '<pre>'; print_r($arResult); echo '</pre>';?>
<?
foreach($arResult["ITEMS"] as $arItem)
	if(array_key_exists("HIDDEN", $arItem))
		echo $arItem["INPUT"];

// Код секции из URL
$section_code = explode ('/', $_SERVER['REQUEST_URI']);
$section_code = $section_code[count($section_code)-2];
$section_id = getSectionIdByCode($section_code);

foreach ($arResult['ITEMS'] as $arItem) {

	// Собираем селект со списокм секций
	if ($arItem['INPUT_NAME'] == 'arrFilter_ff[SECTION_ID]') {
		$section = '<select name="arrFilter_ff[SECTION_ID]" id="arrFilter_ff[SECTION_ID]" size="1"><option value="">- все изделия -</option>';

		if(CModule::IncludeModule("iblock"))
		{
		   $items = GetIBlockSectionList(3, 0, Array("sort"=>"asc"), 99);
		   while($arItem = $items->GetNext())
		   {
		   		if (strpos($_SERVER['REQUEST_URI'], 'catalog') > -1 && ($_GET['arrFilter_ff']['SECTION_ID'] == $arItem['ID'] || $section_id == $arItem['ID'])) $selected = 'selected="selected"'; else $selected = '';
		   		//if (sectionIsEmpty(3, 0, $arItem['ID'])) $disabled = 'disabled="disabled"'; else $disabled = '';
				//Проверка на отсутствие эл-тов в разделе
				$elt_cnt = GetSectionElementCount($arItem['ID']);
				if($elt_cnt <= 0)
					$disabled = 'disabled="disabled"';
				else 
					$disabled = '';

		    	$section .= '<option value="'.$arItem['ID'].'" '.$selected.' '.$disabled.'>'.$arItem['NAME'].'</option>';
		   }
		}

		$section .= '</select>';
	}

	// Собираем селект со списокм металов
	if ($arItem['INPUT_NAME'] == 'arrFilter_pf[METAL_TYPE]') {
		$metal = str_replace('(все)', '- все металы -', $arItem['INPUT']);
	}

	// Собираем селект со списокм вставок
	if ($arItem['INPUT_NAME'] == 'arrFilter_pf[INSERT_TYPE]') {
		$insert = str_replace('(все)', '- все вставки -', $arItem['INPUT']);
	}

	// Собираем цену
	if ($arItem['NAME'] == 'Цена') {
		$find[0] = 'name=';
		$replace[0] = ' class="input-cost" onkeyup="checkPrice($(this));" name=';
		$find[1] = '&nbsp;до&nbsp;';
		$replace[1] = ' <span>-</span> ';
		$find[2] = 'size="5"';
		$replace[2] = '';

		$price = str_replace($find, $replace, $arItem['INPUT']);
	}

	// Собираем размер
	if ($arItem['NAME'] == 'Размер') {
		$size = $arItem['INPUT'];
	}

	// Собираем длину
	if ($arItem['NAME'] == 'Длинна') {
		$length = $arItem['INPUT'];
	}

	// Собираем чекбокс комплект
	if ($arItem['INPUT_NAME'] == 'arrFilter_pf[SET]') {
		$set = '<label>комплект '.str_replace('<label for="arrFilter_pf_SET_109">1</label><br />', '', $arItem['INPUT']).'</label>';
	}
}

?>



<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="/catalog/" method="get" >
	<input type="hidden" name="set_filter" value="Y" />	
			<div class="search-products">
				<div class="strip">
				<div class="search-block">
					<div class="title">Подобрать изделие:</div>
					<div class="wares">
						<?=$section?>
						<input type="hidden" name="arrFilter_ff[INCLUDE_SUBSECTIONS]" id="arrFilter_ff[INCLUDE_SUBSECTIONS]" value="Y" />
						<input type="hidden" name="arrFilter_pf[IS_BUY]" id="arrFilter_pf[IS_BUY]" value="117">
						<input type="hidden" name="arrFilter_pf[IN_STOCK]" id="arrFilter_pf[IN_STOCK]" value="119">
						<?=$metal?>
						
						<?=$insert?>
					</div>
					<div class="size">
						<div class="block-set"><label>Комплект <input type="checkbox" name="arrFilter_pf[SET][]" value="109" id="arrFilter_pf_SET_109" <?php if(isset($_GET['arrFilter_pf']['SET'])) echo 'checked'; ?> /></label></div> 
						<div class="block-set mr-left23"><label>Новинка <input type="checkbox" name="arrFilter_pf[NEW]" value="121" id="" <?php if(isset($_GET['arrFilter_pf']['NEW'])) echo 'checked'; ?> /></label></div> 
						<div class="block-set mr-left23"><label>Распродажа <input type="checkbox" name="arrFilter_pf[SALE]" value="122" id="" <?php if(isset($_GET['arrFilter_pf']['SALE'])) echo 'checked'; ?> /></label></div> 
						<div class="block-amount">
							<label for="amount" id="size_label">размер / длина: </label>
							<input type="text" id="amount" style="border:0; color:#f6931f; font-weight:bold;" />
							<input type="hidden" name="not_active_arrFilter_pf[LENGTH][LEFT]" id="arr_left">
							<input type="hidden" name="not_active_arrFilter_pf[LENGTH][RIGHT]" id="arr_right">

							<div id="progresbar"></div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="cost">
						<label>Цена от</label><div class="row"><?=$price?> <span>рублей</span></div> 
						<input class="buttom-bg" type="submit" name="set_filter"  value="найти" />
						<input class="buttom-bg" type="submit"  name="del_filter"  value="Cбросить" />
						<div class="clear"></div>
					</div> 
				</div>
				</div>
			</div>
</form>

<?php

echo '
<script>
$(document).ready(function() {
';
/*
$db_enum_list = CIBlockProperty::GetPropertyEnum("METAL_TYPE", Array(), Array("IBLOCK_ID"=>3));
while($ar_enum_list = $db_enum_list->GetNext())
{
  $db_important_news = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>3, "PROPERTY"=>array("ACTIVE"=>"Y", "IBLOCK_ACTIVE "=>"Y", "ACTIVE_DATE"=>"Y", "METAL_TYPE"=>$ar_enum_list["ID"])));
  if ($db_important_news->SelectedRowsCount() < 1) {
  	echo '$(\'select[name="arrFilter_pf[METAL_TYPE]"] option[value="'.$ar_enum_list["ID"].'"]\').attr("disabled","disabled");';
  }
}

$db_enum_list = CIBlockProperty::GetPropertyEnum("INSERT_TYPE", Array(), Array("IBLOCK_ID"=>3));
while($ar_enum_list = $db_enum_list->GetNext())
{
  $db_important_news = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>3, "PROPERTY"=>array("ACTIVE"=>"Y", "IBLOCK_ACTIVE "=>"Y", "ACTIVE_DATE"=>"Y", "INSERT_TYPE"=>$ar_enum_list["ID"])));
  if ($db_important_news->SelectedRowsCount() < 1) {
  	echo '$(\'select[name="arrFilter_pf[INSERT_TYPE]"] option[value="'.$ar_enum_list["ID"].'"]\').attr("disabled","disabled");';
  }
}
*/
//Деактивируем металлы, с которыми нет товаров
$db_enum_list = CIBlockProperty::GetPropertyEnum("METAL_TYPE", Array(), Array("IBLOCK_ID"=>3));
while($ar_enum_list = $db_enum_list->GetNext())
{
  $db_important_news = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>3, "=PROPERTY_METAL_TYPE"=>$ar_enum_list["ID"], "ACTIVE"=>"Y", "ACTIVE_DATE" => "Y"));
  if ($db_important_news->SelectedRowsCount() < 1) {
  	echo '$(\'select[name="arrFilter_pf[METAL_TYPE]"] option[value="'.$ar_enum_list["ID"].'"]\').attr("disabled","disabled");';
  }
}
//Деактивируем вставки, с которыми нет товаров
$db_enum_list = CIBlockProperty::GetPropertyEnum("INSERT_TYPE", Array(), Array("IBLOCK_ID"=>3));
while($ar_enum_list = $db_enum_list->GetNext())
{
  $db_important_news = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>3, "=PROPERTY_INSERT_TYPE"=>$ar_enum_list["ID"], "ACTIVE"=>"Y", "ACTIVE_DATE" => "Y"));
  if ($db_important_news->SelectedRowsCount() <= 0) {
  	echo '$(\'select[name="arrFilter_pf[INSERT_TYPE]"] option[value="'.$ar_enum_list["ID"].'"]\').attr("disabled","disabled");';
  }
}

$left = 0;
$right = 150;

if (isset($_GET['arrFilter_pf']['LENGTH']['LEFT']) && $_GET['arrFilter_pf']['LENGTH']['LEFT'] !== '') $left = (int)$_GET['arrFilter_pf']['LENGTH']['LEFT'];
if (isset($_GET['arrFilter_pf']['LENGTH']['RIGHT']) && $_GET['arrFilter_pf']['LENGTH']['RIGHT'] !== '') $right = (int)$_GET['arrFilter_pf']['LENGTH']['RIGHT'];

echo '
var show_ids = ["28", "31", "32", "36"];
if ($.inArray($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val(), show_ids) > -1) {
	$(".block-amount").show();


	if ($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val() == 28) {
		$("#size_label").html("размер: ");

		$( "#progresbar" ).slider({
			min: 15,
			max: 25,
			values: [ '.$left.', '.$right.' ]
		});

		$( "#amount" ).val( "" + $( "#progresbar" ).slider( "values", 0 ) +
			" - " + $( "#progresbar" ).slider( "values", 1 ) );
		$( "#arr_left" ).val($( "#progresbar" ).slider( "values", 0 ));
		$( "#arr_right" ).val($( "#progresbar" ).slider( "values", 1 ));
	}

	if ($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val() == 31) {
		$("#size_label").html("длинна: ");

		$( "#progresbar" ).slider({
			min: 15,
			max: 80,
			values: [ '.$left.', '.$right.' ]
		});

		$( "#amount" ).val( "" + $( "#progresbar" ).slider( "values", 0 ) +
			" - " + $( "#progresbar" ).slider( "values", 1 ) );
		$( "#arr_left" ).val($( "#progresbar" ).slider( "values", 0 ));
		$( "#arr_right" ).val($( "#progresbar" ).slider( "values", 1 ));
	}

	if ($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val() == 32) {
		$("#size_label").html("размер: ");

		$( "#progresbar" ).slider({
			min: 5,
			max: 30,
			values: [ '.$left.', '.$right.' ]
		});

		$( "#amount" ).val( "" + $( "#progresbar" ).slider( "values", 0 ) +
			" - " + $( "#progresbar" ).slider( "values", 1 ) );
		$( "#arr_left" ).val($( "#progresbar" ).slider( "values", 0 ));
		$( "#arr_right" ).val($( "#progresbar" ).slider( "values", 1 ));
	}

	if ($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val() == 36) {
		$("#size_label").html("размер: ");

		$( "#progresbar" ).slider({
			min: 5,
			max: 25,
			values: [ '.$left.', '.$right.' ]
		});

		$( "#amount" ).val( "" + $( "#progresbar" ).slider( "values", 0 ) +
			" - " + $( "#progresbar" ).slider( "values", 1 ) );
		$( "#arr_left" ).val($( "#progresbar" ).slider( "values", 0 ));
		$( "#arr_right" ).val($( "#progresbar" ).slider( "values", 1 ));
	}

} else {
	$(".block-amount").hide();
}
});

$(\'select[name="arrFilter_ff[SECTION_ID]"]\').change(function (){
	var show_ids = ["28", "31", "32", "36"];
	if ($.inArray($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val(), show_ids) > -1) {
		$(".block-amount").show();

		if ($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val() == 28) {
			$("#size_label").html("размер: ");

			$( "#progresbar" ).slider({
				min: 15,
				max: 25,
			values: [ '.$left.', '.$right.' ]
			});

			$( "#amount" ).val( "" + $( "#progresbar" ).slider( "values", 0 ) +
				" - " + $( "#progresbar" ).slider( "values", 1 ) );
			$( "#arr_left" ).val($( "#progresbar" ).slider( "values", 0 ));
			$( "#arr_right" ).val($( "#progresbar" ).slider( "values", 1 ));
		}

		if ($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val() == 31) {
			$("#size_label").html("длинна: ");

			$( "#progresbar" ).slider({
				min: 15,
				max: 80,
			values: [ '.$left.', '.$right.' ]
			});

			$( "#amount" ).val( "" + $( "#progresbar" ).slider( "values", 0 ) +
				" - " + $( "#progresbar" ).slider( "values", 1 ) );
			$( "#arr_left" ).val($( "#progresbar" ).slider( "values", 0 ));
			$( "#arr_right" ).val($( "#progresbar" ).slider( "values", 1 ));
		}

		if ($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val() == 32) {
			$("#size_label").html("размер: ");

			$( "#progresbar" ).slider({
				min: 5,
				max: 30,
			values: [ '.$left.', '.$right.' ]
			});

			$( "#amount" ).val( "" + $( "#progresbar" ).slider( "values", 0 ) +
				" - " + $( "#progresbar" ).slider( "values", 1 ) );
			$( "#arr_left" ).val($( "#progresbar" ).slider( "values", 0 ));
			$( "#arr_right" ).val($( "#progresbar" ).slider( "values", 1 ));
		}

		if ($(\'select[name="arrFilter_ff[SECTION_ID]"]\').val() == 36) {
			$("#size_label").html("размер: ");

			$( "#progresbar" ).slider({
				min: 5,
				max: 25,
			values: [ '.$left.', '.$right.' ]
			});

			$( "#amount" ).val( "" + $( "#progresbar" ).slider( "values", 0 ) +
				" - " + $( "#progresbar" ).slider( "values", 1 ) );
			$( "#arr_left" ).val($( "#progresbar" ).slider( "values", 0 ));
			$( "#arr_right" ).val($( "#progresbar" ).slider( "values", 1 ));
		}


	} else {
		$(".block-amount").hide();
	}
});
</script>
';
?>