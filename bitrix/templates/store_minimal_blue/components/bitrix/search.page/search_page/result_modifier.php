<?php
	CModule::IncludeModule('iblock');
	CModule::IncludeModule('catalog');
	$catalog_iblock_id = 3;
	$server_host = 'http://'.$_SERVER['HTTP_HOST'];
	$catalog_desc_iblock_id = 12;
	$arTemp = $arResult['SEARCH'];
	$arCatalogItems = array();
	$arCatalogItemsKeys = array();
	
	//������ �������� ��������
	$rsSections = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $catalog_iblock_id));
	while($arSection = $rsSections->GetNext())
		$arResult['CATALOG_SECTIONS'][$arSection['ID']] = $arSection;
	
	//������� ������
	foreach($arResult['SEARCH'] as $key => $arItem)
	{
		if($arItem['MODULE_ID'] == 'iblock' && (int)$arItem['ITEM_ID'] > 0 && (int)$arItem['PARAM2'] == $catalog_iblock_id)
		{
			$arCatalogItemsKeys[] = $key;
			$arCatalogItems[] = $arItem['ITEM_ID'];
		}
	}
	//�������� ������ � �������
	if(!empty($arCatalogItems))
	{
		//����������������� ������
		$arReserved = GetReservedProducts();
		
		$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($catalog_iblock_id, array("RETAIL"/*, "BASE"*/));
		$arSelect = array(
			"ID",
			"NAME",
			"CODE",
			"DATE_CREATE",
			"ACTIVE_FROM",
			"CREATED_BY",
			"IBLOCK_ID",
			"IBLOCK_SECTION_ID",
			"DETAIL_PAGE_URL",
			"DETAIL_TEXT",
			"DETAIL_TEXT_TYPE",
			"DETAIL_PICTURE",
			"PREVIEW_TEXT",
			"PREVIEW_TEXT_TYPE",
			"PREVIEW_PICTURE",
			"PROPERTY_*",
		);
		$arFilter = array(
			"IBLOCK_ID" => $catalog_iblock_id, 
			'ID' => $arCatalogItems,
			"ACTIVE_DATE" => "Y",
			"ACTIVE" => "Y",
			"=PROPERTY_IS_BUY" => 117,
			"=PROPERTY_IN_STOCK" => 119
		);
		foreach($arResult["PRICES"] as $key => $value)
		{
			$arSelect[] = $value["SELECT"];
			//$arFilter["CATALOG_SHOP_QUANTITY_".$value["ID"]] = 1;
		}
		$rsElements = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while($obElement = $rsElements->GetNextElement())
		{
			$arElement = $obElement->GetFields();
			//$arElement['PROPERTIES'] = $obElement->GetProperties();
			$arElement['PREVIEW_PICTURE'] = CFile::GetFileArray((int)$arElement['PREVIEW_PICTURE']);
			$arElement["PRICES"] = CIBlockPriceTools::GetItemPrices($catalog_iblock_id, $arResult["PRICES"], $arElement, "Y");
			$arItem["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem);
			
			$arElement["BUY_URL"] = htmlspecialchars('/catalog/?action=BUY&id='.$arElement["ID"]);
			$arElement["ADD_URL"] = htmlspecialchars('/catalog/?action=ADD2BASKET&id='.$arElement["ID"]);
			$arElement["COMPARE_URL"] = htmlspecialchars($APPLICATION->GetCurPageParam("action=ADD_TO_COMPARE_LIST&id=".$arElement["ID"], array("action", "id")));
			
			if(isset($arReserved[$arElement['ID']]))
				$arElement['RESERVED'] = 'Y';
			
			$arResult['CATALOG_ITEMS'][$arElement['ID']] = $arElement;
		}
	}
	//������� ������, ������ ������
	foreach($arResult['SEARCH'] as $key => $arItem)
	{
		if($arItem['MODULE_ID'] == 'iblock' && (int)$arItem['ITEM_ID'] > 0)
		{
			//���, ��� ��������� � ��������
			if((int)$arItem['PARAM2'] == $catalog_iblock_id)
			{
				if(isset($arResult['CATALOG_ITEMS'][(int)$arItem['ITEM_ID']]))
				{
					$arResult['CATALOG_ITEMS'][(int)$arItem['ITEM_ID']]['SEARCH_DATA'] = $arItem;
				}
				unset($arResult['SEARCH'][$key]);
			}
			//��� ��� ��������� � �������� ��������
			elseif((int)$arItem['PARAM2'] == $catalog_desc_iblock_id)
			{
				$rsDesc = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $catalog_desc_iblock_id, "ID" => (int)$arItem['ITEM_ID']), false, false, array("ID", "PROPERTY_SECTION_ID"));
				if($arDesc = $rsDesc->Fetch())
					$arResult['SEARCH'][$key]['URL'] = $server_host.$arResult['CATALOG_SECTIONS'][$arDesc['PROPERTY_SECTION_ID_VALUE']]['SECTION_PAGE_URL'].$arItem['URL'];
			}
		}
	}
	
?>