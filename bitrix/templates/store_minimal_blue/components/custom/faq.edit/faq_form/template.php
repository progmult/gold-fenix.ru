<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="faq-question">
	<p class="faq-title">Ваш вопрос:</h2>
	<?if(!empty($arResult['ERROR'])):?>
		<?foreach($arResult['ERROR'] as $error):?>
			<p class="error"><?=$error?></p>
		<?endforeach;?>
	<?endif;?>
	<?if($arResult['RESULT'] == 'Y'):?>
		<p class="success">Ваш вопрос добавлен и будет опубликован после одобрения администратором.</p>
	<?endif;?>
	<form action="" method="POST">
		<p><textarea rows="10" id="question" name="question"><?=$arResult['DATA']['QUESTION']?></textarea></p>
		<p class="faq-title">Введите код с картинки:</p>
		<div class="captcha-field">
			<img class="captcha-img-field" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
			<br />
			<input class=" captcha-text-field" type="text" name="captcha_word" size="30" maxlength="50" value="">
			<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
		</div>
		
		<p><input type="submit" class="uniform-btnSubmit" name="q_submit" value="Отправить" /></p>
	</form>
</div>
<?
	/*
	echo '<pre>'; print_r($arResult); echo '</pre>';
	echo '<pre>'; print_r($_POST); echo '</pre>';
	echo '<pre>'; print_r($_REQUEST); echo '</pre>';
	*/
?>