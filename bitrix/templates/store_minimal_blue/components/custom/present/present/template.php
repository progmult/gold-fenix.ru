<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult["ERROR_MESSAGE"]) || strlen($arResult["OK_MESSAGE"]) > 0):?>
	<script>
		jQuery(function($){
			$('#modal_present').dialog('open');
		});
	</script>
<?endif;?>
<div class="present_form">
	<h2>Хочу в подарок</h2>
	<?if(!empty($arResult["ERROR_MESSAGE"]))
	{
		foreach($arResult["ERROR_MESSAGE"] as $v)
			ShowError($v);
	}
	if(strlen($arResult["OK_MESSAGE"]) > 0)
	{
		?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
	}
	?>
	<div id="product_description">
		<div class="block-product">
			<div class="product present-product">
			  <div class="img small-img-cont">
				  <img src="<?=$arResult['PRODUCT']['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arResult['PRODUCT']['NAME']?>" title="<?=$arResult['PRODUCT']['NAME']?>" />
			  </div>
			  <div class="price"><?=$arResult['PRODUCT']['PRODUCT_PRICE']?> р</div>
			  <a class="name" href="<?=$arResult['PRODUCT']['DETAIL_PAGE_URL']?>"><?=$arResult['PRODUCT']['NAME']?></a>
			</div>
		</div>
	</div>
	<div id="present_form">
		<form action="<?=$APPLICATION->GetCurPage()?>" method="POST">
		<?=bitrix_sessid_post()?>
			<div class="present-field">
				<p class="present-title"><span class="mf-req">*</span>Кто хочет подарок:</p>
				<p class="present-input"><input type="text" placeholder="Ваше имя" name="user_name" value="<?=$arResult["USER_NAME"]?>"></p>
			</div>
			
			<div class="present-field">
				<p class="present-title"><span class="mf-req">*</span>Ваша электронная почта:</p>
				<p class="present-input"><input type="text" name="user_email" value="<?=$arResult["USER_EMAIL"]?>"></p>
			</div>
			
			<div class="present-field">
				<p class="present-title">Сопроводительный текст:</p>
				<p class="present-input"><textarea name="MESSAGE" rows="5" cols="40"><?=$arResult["MESSAGE"]?></textarea></p>
			</div>
			
			<div class="present-field">
				<p class="present-title"><span class="mf-req">*</span>Кому отправить:</p>
				<p class="present-input"><input type="text" placeholder="Имя получателя" name="present_to_name" value="<?=$arResult["PRESENT_TO_NAME"]?>"></p>
			</div>
			
			<div class="present-field">
				<p class="present-title"><span class="mf-req">*</span>E-mail получателя:</p>
				<p class="present-input"><input type="text" name="present_to_email" value="<?=$arResult["PRESENT_TO_EMAIL"]?>"></p>
			</div>
			
			<?if($arParams["USE_CAPTCHA"] == "Y"):?>
			<div class="mf-captcha">
				<div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
				<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
				<div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
				<input type="text" name="captcha_word" size="30" maxlength="50" value="">
			</div>
			<?endif;?>
			<input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
		</form>
	</div>
</div>