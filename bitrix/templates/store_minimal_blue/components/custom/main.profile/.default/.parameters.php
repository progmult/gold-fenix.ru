<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
<?/*
	$arRequiredFields = array(
		"LAST_NAME" => "Фамилия",
		"NAME" => "Имя",
		"SECOND_NAME" => "Отчество",
		"PERSONAL_PHONE" => "Телефон",
		"PERSONAL_CITY" => "Город",
		"PERSONAL_STREET" => "Улица, дом",
		"UF_SUBWAY" => "Станция метро",
		"WORK_COMPANY" => "Название организации",
		"WORK_POSITION" => "Должность",
		"WORK_WWW" => "Сайт",
		"UF_BANK" => "Банк",
		"UF_ACCOUNT" => "Корр. счет",
		"UF_BIK" => "БИК",
		"UF_INN" => "ИНН",
		"UF_KPP" => "КПП",
		"UF_ACCOUNT2" => "Расч. счет"
	);
*/?>
$arTemplateParameters = array(
	"USER_PROPERTY_NAME"=>array(
		"NAME" => GetMessage("USER_PROPERTY_NAME"),
		"TYPE" => "STRING",
		"DEFAULT" => "",	
	),
	"SUPPLLIER_REQUIRED_FIELDS"=>array(
		"NAME" => 'Обязательные поля поставщика',
		"TYPE" => "LIST",
		"VALUES" => $arRequiredFields,
		"MULTIPLE" => "Y",
		"DEFAULT" => array(),
	),
);
?>