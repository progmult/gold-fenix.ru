<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
//echo "<pre>"; print_r($arResult); echo "</pre>";
//exit();
//echo "<pre>"; print_r($_SESSION); echo "</pre>";
?>
<?//echo '<pre>'; print_r($arParams); echo '</pre>';?>
<?=ShowError($arResult["strProfileError"]);?>
<?

if ($arResult['DATA_SAVED'] == 'Y') {
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
	$arResult['DATA_SAVED'] = 'N';
	$_SESSION['profile_saved'] = 'success';
	header ('Location: /personal/profile/?saved=1');
}

if ($_GET['saved'] == 1) {
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
}

?>

<?
	//Отметка звездочкой обязательных полей для поставщика
	if(IsUserSupplier())
		$required_field_sign = '<span class="required-field">*</span>';
?>
<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
<input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
<input type="hidden" name="EMAIL" value=<?=$arResult["arUser"]["EMAIL"]?> />

<div class="content-form profile-form">
	<div class="fields">

		<div class="field field-lastname">	
			<label class="field-title"><?=$required_field_sign?><?=GetMessage('LAST_NAME')?></label>
			<div class="form-input"><input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" /></div>
		</div>
		<div class="field field-firstname">
			<label class="field-title"><?=$required_field_sign?><?=GetMessage('NAME')?></label>
			<div class="form-input"><input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" /></div>
		</div>
		<div class="field field-secondname">	
			<label class="field-title"><?=$required_field_sign?><?=GetMessage('SECOND_NAME')?></label>
			<div class="form-input"><input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" /></div>
		</div>
	</div>
	<div class="fields">
		<div class="field field-password_new">	
			<label class="field-title"><?=GetMessage('NEW_PASSWORD_REQ')?></label>

			<div class="form-input"><input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" /></div>
			
		</div>
		<div class="field field-password_confirm">
			<label class="field-title"><?=GetMessage('NEW_PASSWORD_CONFIRM')?></label>
			<div class="form-input"><input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" /></div>
			
		</div>
	</div>
		<div class="field field-phone">
			<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maskedinput-1.2.2.js"></script>
			<script>
				jQuery(function($){
					$.mask.definitions['~']='[+-]';
					$('#personal_phone_field').mask('+7(999)999-99-99').addClass('masked_field');
					$('#personal_phone_field').live('focus', function(){
						if(!$(this).hasClass('masked_field'))
						{
							$('#personal_phone_field').mask('+7(999)999-99-99').addClass('masked_field');
						}
					});
				});
			</script>
			<label class="field-title"><?=$required_field_sign?>Телефон</label>
			<div class="form-input"><input type="text" id="personal_phone_field" name="PERSONAL_PHONE" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title"><?=$required_field_sign?><?=GetMessage('USER_CITY')?></label>
			<div class="form-input"><input type="text" name="PERSONAL_CITY" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" /></div>
		</div>
		<?/*
		<div class="field field-phone">
		<?php
			$APPLICATION->IncludeComponent("custom:metro", "", array(
				'iblock_id' => 9,
				'field_name' => 'UF_METRO',
				'val' => $arResult["arUser"]["UF_METRO"],
			));
		?> 
		
		</div>
		*/?>
		<div class="field field-phone">
			<label class="field-title"><?=$required_field_sign?><?=GetMessage("USER_STREET")?></label>
			<div class="form-input"><input type="text" name="PERSONAL_STREET" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_STREET"]?>" /></div>
		</div>

<?php if (in_array(8, CUser::GetUserGroup($arResult['arUser']['ID']))): ?>
		<?if((int)$arResult["arUser"]["UF_METRO_STATION"] > 0):?>
			<? $bMetro = true; ?>
			<script>
				jQuery(function($){
					var selected_metro = $(".field-metro .form-input-metro-select select option:selected").text();
					$(".field-metro .metro-station-string").val(selected_metro);
					
					$(".metro-select-show").click(function(){
						$(".field-metro .form-input-metro-select").slideToggle("fast");
						return false;
					});
				});
			</script>
		<?endif;?>
		<div class="field field-phone field-metro">
			<label class="field-title"><?=$required_field_sign?>Станция метро</label>
			<?if($bMetro):?>
				<div class="form-input">
					<input class="metro-station-string" type="text" maxlength="50" value="" readonly="readonly" />
				</div>
				<p class="metro-station-info">Если вы хотите изменить станцию метро, кликните <a class="metro-select-show">сюда</a></p>
			<?endif;?>
			<div class="form-input form-input-metro-select"<?if($bMetro):?> style="display:none;"<?endif;?>>
				<?php
					$GLOBALS['APPLICATION']->IncludeComponent("custom:metro", "supplier_profile", array(
						'iblock_id' => 9,
						'field_name' => 'UF_METRO_STATION',
						'val' => $arResult["arUser"]["UF_METRO_STATION"],
					));
				?> 
			</div>
		</div>
		<?/*
		   * Для пользовательского свойства
		   
		<div class="field field-phone field-metro">
			<label class="field-title"><?=$required_field_sign?><?=$arResult['USER_PROPERTIES']['DATA']['UF_SUBWAY']['EDIT_FORM_LABEL']?></label>
			<div class="form-input">
				<?$APPLICATION->IncludeComponent(
						"bitrix:system.field.edit",
						$arResult['USER_PROPERTIES']['DATA']['UF_SUBWAY']["USER_TYPE"]["USER_TYPE_ID"],
						array(
							"bVarsFromForm" => $arResult["bVarsFromForm"], 
							"arUserField" => $arResult['USER_PROPERTIES']['DATA']['UF_SUBWAY']), 
							null,
							array("HIDE_ICONS"=>"Y")
				);
				?>
			</div>
		</div>
		*/?>
		<div class="field field-phone">
			<label class="field-title">Название организации</label>
			<div class="form-input">
				<input type="text" name="WORK_COMPANY" maxlength="50" value="<?=$arResult["arUser"]["WORK_COMPANY"]?>" />
			</div>
		</div>
		<div class="field field-phone">
			<label class="field-title">Должность</label>
			<div class="form-input"><input type="text" name="WORK_POSITION" maxlength="50" value="<?=$arResult["arUser"]["WORK_POSITION"]?>" /></div>
		</div>		
		<div class="field field-phone">
			<label class="field-title">Сайт</label>
			<div class="form-input"><input type="text" name="WORK_WWW" maxlength="50" value="<?=$arResult["arUser"]["WORK_WWW"]?>" /></div>
		</div>


		<div class="field field-phone">
			<label class="field-title">Банк</label>
			<div class="form-input"><input type="text" name="UF_BANK" maxlength="50" value="<?=$arResult["arUser"]["UF_BANK"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">Корр. счет</label>
			<div class="form-input"><input type="text" name="UF_ACCOUNT" maxlength="50" value="<?=$arResult["arUser"]["UF_ACCOUNT"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">БИК</label>
			<div class="form-input"><input type="text" name="UF_BIK" maxlength="50" value="<?=$arResult["arUser"]["UF_BIK"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">ИНН</label>
			<div class="form-input"><input type="text" name="UF_INN" maxlength="50" value="<?=$arResult["arUser"]["UF_INN"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">КПП</label>
			<div class="form-input"><input type="text" name="UF_KPP" maxlength="50" value="<?=$arResult["arUser"]["UF_KPP"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title">Расч. счет</label>
			<div class="form-input"><input type="text" name="UF_ACCOUNT2" maxlength="50" value="<?=$arResult["arUser"]["UF_ACCOUNT2"]?>" /></div>
		</div>
		<div class="field field-phone">
			<label class="field-title"><?=$required_field_sign?>Контактное лицо</label>
			<div class="form-input"><input type="text" name="UF_CONTACT_PERSON" maxlength="50" value="<?=$arResult["arUser"]["UF_CONTACT_PERSON"]?>" /></div>
		</div>
<?php endif;?>
</div>

<?php
//print '<pre>';
//print_R ($arResult);
?>

<div class="content-form profile-form">
	<div class="button-submit"><input name="save" value="<?=GetMessage("MAIN_SAVE")?>"class="input-submit" type="submit"></div>
</div>
</form>