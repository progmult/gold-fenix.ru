<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
<?if($USER->IsAuthorized()):?>
	<p><?echo GetMessage("MAIN_REGISTER_AUTH");?></p>
<?else:?>
	<?if(!empty($arResult["VALUES"]) && count($arResult["ERRORS"]) <= 0 && $arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
		<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT");?></p>
	<?else:?>
		<div id="user_register_form">
			<form id="signup_form" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform_user" enctype="multipart/form-data">
				<?=bitrix_sessid_post()?>
				<div class="content-form profile-form">
					<input type="hidden" name="user_register" value="1" />
					<?foreach($arResult["ERRORS"] as $key => $error):?>
						<?if($key == 'EMAIL_INVAILD'):?>
							<p style="color:red; padding:0; margin:0;"><?=$error?></p>
						<?else:?>
							<p style="color:red; padding:0; margin:0;"><?=str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);?></p>
						<?endif;?>
					<?endforeach;?>
					<?if($arResult["BACKURL"] <> ''):?>
						<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
					<?endif;?>
					
					<div class="fields">
						
						<div class="field field-phone">
							<label class="field-title">Email<span class="required-field">*</span></label>
							<div class="form-input">
								<input name="REGISTER[EMAIL]" type="text" value="<?=$arResult["VALUES"]['EMAIL']?>"<?if(isset($arResult['ERRORS']['EMAIL']) || isset($arResult['ERRORS']['EMAIL_INVAILD'])):?> class="red-border"<?endif;?> />
							</div>
						</div>
						
						<div class="field field-phone">
							<label class="field-title">Пароль<span class="required-field">*</span></label>
							<div class="form-input">
								<input name="REGISTER[PASSWORD]" type="password" value="<?=$arResult["VALUES"]['PASSWORD']?>"<?if(isset($arResult['ERRORS']['PASSWORD'])):?> class="red-border"<?endif;?> />
							</div>
						</div>
						
						<div class="field field-phone">
							<label class="field-title">Подтверждение пароля<span class="required-field">*</span></label>
							<div class="form-input">
								<input name="REGISTER[CONFIRM_PASSWORD]" type="password" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>"<?if(isset($arResult['ERRORS']['CONFIRM_PASSWORD'])):?> class="red-border"<?endif;?> />
							</div>
						</div>
						
						<div class="field field-phone">
							<label class="field-title">Имя</label>
							<div class="form-input">
								<input name="REGISTER[NAME]" type="text" value="<?=$arResult["VALUES"]['NAME']?>"<?if(isset($arResult['ERRORS']['NAME'])):?> class="red-border"<?endif;?> />
							</div>
						</div>
						
						<div class="field field-phone">
							<label class="field-title">Пользовательское соглашение<span class="required-field">*</span></label>
							<div class="form-input">
								<div class="row" style="overflow:hidden;">
									<?/*<input type="hidden" value="0" name="UF_TOS">*/?>
									<div style="float:left; padding:0 10px 0 0;">
									<input type="checkbox" value="on" name="REGISTER[UF_TOS]"<?=$arResult["VALUES"]['UF_TOS'] == "on" ? " checked=\"checked\"" : ""?><?if(isset($arResult['ERRORS']['UF_TOS'])):?> class="red-border"<?endif;?> />
									</div>
									<label for="REGISTER[UF_TOS]" style="display:block;float:left;">Я принимаю условия <a rel="nofollow" href="/polzovatelskoe-soglashenie/client/" target="_blank">пользовательского соглашения</a></label>
								</div>
							</div>
						</div>
						<div class="field field-phone">
							<div class="form-input">
								<div class="row" style="margin-top: 20px;">
									<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
									<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" style="border: 1px solid #ABADB3;" />
									<p>Мы должны убедиться ,что вы не Вселенский Аннигилятор Ландшафтный Легкий Интеллектуальный.</p>
									<input type="text" name="captcha_word" value=""<?if(isset($arResult['ERRORS'][0])):?> class="red-border"<?endif;?> />
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="content-form profile-form">
					<div class="button-submit">
						<input type="submit" id="register_user_submit_button" name="register_user_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
					</div>
				</div>
			</form>
		</div>
	<?endif;?>
<?endif;?>