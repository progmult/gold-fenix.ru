<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
<style>
	.register_tab_block{
		display:none;
	}
	.register_tab_block.active_register_tab_block{
		display:block;
	}
</style>
<?if(!empty($arResult['ERRORS']) && (int)$_REQUEST['modal_register_window'] > 0):?>
	<script>
		jQuery(function($){
			 $('#modal_signup').dialog({
                autoOpen: false,
                draggable: false,
                modal: true,
                resizable: false,
                width: 460,
                //show: 'scale',
                //hide: 'scale'
            });
			
			$('#modal_signup').dialog( "open" );
			
			<?if((int)$_REQUEST['user_register'] > 0):?>
			$(".modal_tabs ul li").removeClass('register_active_tab');
			$(".modal_tabs ul li[rel=register_tab_1]").addClass('register_active_tab');
			
			$(".register_tab_block").removeClass('active_register_tab_block');
			$("#register_tab_1").addClass('active_register_tab_block');
			<?endif;?>
		});
	</script>
<?endif;?>

<form id="signup_form" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform_user" enctype="multipart/form-data">
<?=bitrix_sessid_post()?>
<input type="hidden" name="modal_register_window" value="1" />
<input type="hidden" name="user_register" value="1" />

<?if(!empty($arResult['ERRORS'])):?>
	<div style="display:none;">
		<?echo '<pre>'; print_r($arResult['ERRORS']); echo '</pre>';?>
	</div>
	<div class="register_errors">
	<?foreach($arResult["ERRORS"] as $key => $error):?>
		<?if($key == 'EMAIL_INVAILD'):?>
			<p style="color:red; padding:0; margin:0;"><?=$error?></p>
		<?else:?>
			<p style="color:red; padding:0; margin:0;"><?=str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);?></p>
		<?endif;?>
	<?endforeach;?>
	</div>
<?endif;?>
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif;?>
	<div class="row">
		<label for="REGISTER[EMAIL]">Email<span class="required-field">*</span></label>
		<input name="REGISTER[EMAIL]" type="text" value="<?=$arResult["VALUES"]['EMAIL']?>"<?if(isset($arResult['ERRORS']['EMAIL']) || isset($arResult['ERRORS']['EMAIL_INVAILD'])):?> class="red-border"<?endif;?> />
	</div>
	<div class="row">
		<label for="REGISTER[PASSWORD]">Пароль<span class="required-field">*</span></label>
		<input name="REGISTER[PASSWORD]" type="password" value="<?=$arResult["VALUES"]['PASSWORD']?>"<?if(isset($arResult['ERRORS']['PASSWORD'])):?> class="red-border"<?endif;?> />
	</div>
	<div class="row">
		<label for="REGISTER[CONFIRM_PASSWORD]">Подтверждение пароля<span class="required-field">*</span></label>
		<input name="REGISTER[CONFIRM_PASSWORD]" type="password" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>"<?if(isset($arResult['ERRORS']['CONFIRM_PASSWORD'])):?> class="red-border"<?endif;?> />
	</div>
	
	<div class="row">
		<label for="REGISTER[NAME]">Имя</label>
		<input name="REGISTER[NAME]" type="text" value="<?=$arResult["VALUES"]['NAME']?>"<?if(isset($arResult['ERRORS']['NAME'])):?> class="red-border"<?endif;?> />
	</div>
	
	<div class="row" style="overflow:hidden;">
		<?/*<input type="hidden" value="0" name="UF_TOS">*/?>
		<div style="float:left; padding:0 10px 0 0;">
		<input type="checkbox" value="on" name="REGISTER[UF_TOS]"<?=$arResult["VALUES"]['UF_TOS'] == "on" ? " checked=\"checked\"" : ""?><?if(isset($arResult['ERRORS']['UF_TOS'])):?> class="red-border"<?endif;?> />
		</div>
		<label for="REGISTER[UF_TOS]" style="display:block;float:left;">Я принимаю условия <a rel="nofollow" href="/polzovatelskoe-soglashenie/client/" target="_blank">пользовательского соглашения</a><span class="required-field">*</span></label>
	</div>

	<div class="row" style="margin-top: 20px;">
		<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" style="border: 1px solid #ABADB3;" />
		<p><NOINDEX>Мы должны убедиться ,что вы не Вселенский Аннигилятор Ландшафтный Легкий Интеллектуальный.</NOINDEX></p>
		<input type="text" name="captcha_word" value=""<?if(isset($arResult['ERRORS'][0])):?> class="red-border"<?endif;?> />
	</div>
	<div class="links links-supplier">
		
		<div style="text-align: right"><a rel="nofollow" href="#">Забыли пароль?</a></div>
	</div>
	<a rel="nofollow" href="#" id="btn_submit2" class="user_btn">Регистрация</a>
	<script>
		$(document).ready(function(){
			$('.user_btn').click(function(){
				$('#register_user_submit_button').click();
			})
		})
	</script>
	<input type="submit" id="register_user_submit_button" name="register_user_submit_button" style="display: none;" value="<?=GetMessage("AUTH_REGISTER")?>" />
</form>