<?php 
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	if($arResult['is_xhr']) {
		$APPLICATION->RestartBuffer();
		die(json_encode($arResult));
	}  
?>

<div class="metro-station-c">
<select id="metro-station" class="not-uniform" name="<?=$arResult['field_name']?>" size="10">
    <option value="">Нет</option>
	<? foreach( $arResult['items'] as $item ): ?>
	<?php
		$selected = ($item['id'] === $arResult['val']) ? 'selected' : '';
	?>
	<option class="station" data-city="<?=$item['city']?>" value="<?=$item['id']?>" <?=$selected?>><?=$item['name']?></option>	
	<? endforeach; ?>
</select>
</div>