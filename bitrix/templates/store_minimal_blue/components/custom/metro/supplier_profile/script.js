/**
 * @author Denis
 */
/*from wiki*/
/*
var cuthalf=150;
var buf=new Array((cuthalf * 2) - 1);
 
function min3(a, b, c) {
 return Math.min(Math.min(a,b),c);
}
 
function LeveDist(s, t) {
  var i, j, m, n, cost, flip, result;
  s = s.substr(0,cuthalf);
  t = t.substr(0,cuthalf);
  m = s.length;
  n = t.length;
  if (m == 0)
    result = n;
  else if (n == 0)
    result = m;
  else {
    flip = false;
    for (i = 0; i <= n; i++)
      buf[i] = i;
    for (i = 1; i<=m; i++) {
      if (flip)
        buf[0] = i;
      else
        buf[cuthalf] = i;
      for (j = 1; j<=n; j++) {
        if (s.charAt(i-1) == t.charAt(j-1))
          cost = 0;
        else
          cost = 1;
        if (flip)
          buf[j] = min3((buf[cuthalf + j] + 1),
            (buf[j - 1] + 1),
            (buf[cuthalf + j - 1] + cost))
        else
          buf[cuthalf + j] = min3((buf[j] + 1),
            (buf[cuthalf + j - 1] + 1),
            (buf[j - 1] + cost));
      }
      flip = !flip;
    }
    if (flip)
      result = buf[cuthalf + n];
    else
      result = buf[n];
  }
  return result;
}

$(function(){
	function keypress(){
		var
			v = $('input[name=PERSONAL_CITY]').val() || $('select[name=ORDER_PROP_5]').find('option:selected').text();

		$('.station').each(function(){
			var
				t = $(this),
				c = t.attr('data-city');
			
			if(LeveDist(v, c) > 3) {
				t.hide().attr('data-visible', '');
			} else {
				t.show().attr('data-visible', 'visible');
			}
		});
		/*
		if($('.station').length === $('.station[data-visible!="visible"]').length) {
			$('.metro-station-c').hide();
		} else {
			$('.metro-station-c').show();
		}
		
		$.uniform.update();
	}
	//$('.station').hide();
	keypress();
	$('input[name=PERSONAL_CITY]').keypress(keypress);
})
*/
