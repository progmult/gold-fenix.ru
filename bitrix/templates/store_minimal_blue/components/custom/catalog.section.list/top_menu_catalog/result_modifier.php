<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule('iblock');
	//echo '<pre>'; print_r($arResult['SECTIONS']); echo '</pre>';
	foreach($arResult['SECTIONS'] as $key => $section)
	{
		if((int)$section['ELEMENT_CNT'] > 0)
		{
			$real_cnt = 0;
			$rsElements = CIBlockElement::GetList(
				array(), 
				array(
					"IBLOCK_ID" => $section['IBLOCK_ID'], 
					"=SECTION_ID" => $section['ID'], 
					"INCLUDE_SUBSECTIONS" => "Y", 
					"ACTIVE" => "Y"
				)
			);
			while($obElement = $rsElements->GetNextElement())
			{
				$arElement['PROPERTIES'] = $obElement->GetProperties();
				if($arElement['PROPERTIES']['IN_STOCK']['VALUE_ENUM_ID'] == 119 && $arElement['PROPERTIES']['IS_BUY']['VALUE_ENUM_ID'] == 117)
				{
					$real_cnt++;
				}
			}
			$arResult['SECTIONS'][$key]['REAL_COUNT'] = $real_cnt;
		}
	}
?>