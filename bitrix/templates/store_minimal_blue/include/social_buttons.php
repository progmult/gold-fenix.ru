<div class="social_buttons">
	<!--VK-->
	<!--FB-->
	<!--Одноклассники-->
	<!--Twitter-->
	<!--google-->

	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/include/social.css");?>
	<p class="socials-title">Поделиться</p>
	<div class="catalog-detail-line"></div>
	
	<div class="share">
		<div class="share42init"></div>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/share42/share42.js"></script>
	</div>
	
	<p class="socials-title">Мне нравится</p>
	<div class="catalog-detail-line"></div>
	
	<div class="likes">
		<table class="likes-table">
			<tr>
				<td>
					<div class="like-button-cont like-but-facebook">
						<div id="fb-root"></div>
						<script>
							(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));
						</script>
						<div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true"></div>
					</div>
				</td>
				<td>
					<div class="like-button-cont like-but-twitter">
						<script>
						!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
						</script>
						<a rel="nofollow" href="https://twitter.com/share" class="twitter-share-button" data-lang="ru">Твитнуть</a>
					</div>
				</td>
				<td>
					<div class="like-button-cont like-but-google">
						<script src="https://apis.google.com/js/platform.js" async defer></script>
						<g:plusone></g:plusone>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>