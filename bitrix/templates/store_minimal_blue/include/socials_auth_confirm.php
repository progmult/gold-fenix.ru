<?if(isset($_GET['reg_action'])):?>
	
	<?if($_GET['reg_action'] == 'check_email' && isset($_GET['mail']) && isset($_GET['check_id'])):?>
		<?
			CModule::IncludeModule('iblock');
			$bOk = false;
			$email_send = '';
			$rsCheck = CIBlockElement::GetByID((int)$_GET['check_id']);
			if($obCheck = $rsCheck->GetNextElement())
			{
				$bOk = true;

				$arCheck['PROPERTIES'] = $obCheck->GetProperties();
				$email_send = $arCheck['PROPERTIES']['EMAIL']['VALUE'];
				//Проверяем e-mail
				if((int)$_GET['email_variant'] == 2)
				{
					if(check_email($_GET['email_self']))
					{
						$email_send = htmlspecialchars($_GET['email_self']);
						
						if($email_send != $arCheck['PROPERTIES']['EMAIL']['VALUE'])
						{
							//Если юзер ввел другой e-mail - проверяем, есть ли такой аккаунт
							$rsUser = CUser::GetList(
								($by = "email"),
								($order = "desc"),
								array(
									"EMAIL" => $email_send
								),
								array(
									"SELECT" => array("UF_*")
								)
							);
							//Если есть - удаляем только что созданный новый аккаунт и отправляем письмо на новый
							if((int)$rsUser->SelectedRowsCount() > 0)
							{
								$arUser = $rsUser->Fetch();
								if(CUser::Delete((int)$arCheck['PROPERTIES']['USER_ID']['VALUE']))
									CIBlockElement::SetPropertyValuesEx((int)$_GET['check_id'], false, array('USER_ID' => (int)$arUser['ID']));
								else
									LocalRedirect('/?reg_action=check_email&error_code=2&check_id='.(int)$_GET['check_id']);
							}
							//Если нет - обновляем данные аккаунта пользователя
							else
							{
								$obUser = new CUser;
								$userFields = Array(
									"EMAIL" => $email_send,
									"LOGIN" => $email_send
								);
								$obUser->Update((int)$arCheck['PROPERTIES']['USER_ID']['VALUE'], $userFields);
								
								CIBlockElement::SetPropertyValuesEx((int)$_GET['check_id'], false, array('EMAIL' => $email_send, 'EMAIL_USER' => $email_send));
							}
						}
						//CIBlockElement::SetPropertyValuesEx((int)$_GET['check_id'], false, array('EMAIL_USER' => $_GET['email_self']));
					}
					else
						LocalRedirect('/?reg_action=check_email&error_code=1&check_id='.(int)$_GET['check_id']);
				}
				//Отправляем письмо
				if(strlen($email_send) > 0)
				{
					$arEventFields = array(
						'EMAIL_TO' => $email_send,
						'LINK' => 'http://'.$_SERVER['HTTP_HOST'].'/?reg_action=confirm_email&check_code='.$arCheck['PROPERTIES']['CHECK_CODE']['VALUE']
					);
					CEvent::Send("ACCOUNT_CONFIRM", SITE_ID, $arEventFields);
				}
				
			}
		?>
		<?if($bOk):?>
			 <script>
				$(document).ready(function(){

					$('#social_check_window').dialog({
						autoOpen: true,
						draggable: false,
						modal: true,
						resizable: false,
						width: 425,
						//show: 'scale',
						//hide: 'scale'
					});
					
					$("#social_check_close").click(function(){
						$('#social_check_window').dialog("close");
					});
				});
			</script>
			<div id="social_check_window" style="display:none;">
				<div id="social_check_close" class="modal_close_button"></div>
				<div id="social_check_body" style="padding:20px;">
					<p style="font-size: 18px; color:#444; text-align:center;">Подтверждение аккаунта</p>
					<p>Ваш аккаунт необходимо подтвердить. На указаный e-mail (<?=$email_send?>) был отправлен код подтверждения Вашего аккаунта.</p>
					<p>Процедура подтверждения проводится один раз.</p>
				</div>
			</div>
		<?endif;?>
	<?elseif($_GET['reg_action'] == 'check_email' && (int)$_GET['check_id'] > 0):?>
		<?
			CModule::IncludeModule('iblock');
			$bOk = false;
			$rsCheck = CIBlockElement::GetByID((int)$_GET['check_id']);
			if($obCheck = $rsCheck->GetNextElement())
			{
				$bOk = true;
				$arCheck['PROPERTIES'] = $obCheck->GetProperties();
				
			}
		?>
		<?if($bOk):?>
			 <script>
				$(document).ready(function(){

					$('#social_check_window').dialog({
						autoOpen: true,
						draggable: false,
						modal: true,
						resizable: false,
						width: 500,
						//show: 'scale',
						//hide: 'scale'
					});
					
					$("#social_check_close").click(function(){
						$('#social_check_window').dialog("close");
					});
				});
			</script>
			<div id="social_check_window" style="display:none;">
				<div id="social_check_close" class="modal_close_button"></div>
				<div id="social_check_body" style="padding:20px;">
					<p style="font-size: 18px; color:#444; text-align:center;">Подтверждение E-mail</p>
					<?if(isset($_GET['error_code']) && (int)$_GET['error_code'] == 1):?>
						<p style="color:red;">Введен некорректный e-mail</p>
					<?endif;?>
					<form method="get" action="">
						<input type="hidden" name="reg_action" value="check_email" />
						<input type="hidden" name="mail" value="send" />
						<input type="hidden" name="check_id" value="<?=(int)$_GET['check_id']?>" />
						<table>
							<tr>
								<td><input type="radio" checked="checked" name="email_variant" value="1" /></td>
								<td><input type="text" style="width:180px;" readonly="readonly" name="email_soc" value="<?=$arCheck['PROPERTIES']['EMAIL']['VALUE']?>" />&nbsp; - e-mail, полученный из соц.сети</td>
							</tr>
							<tr>
								<td><input type="radio" name="email_variant" value="2" /></td>
								<td><input type="text" style="width:180px;" name="email_self" value="" />&nbsp; - другой</td>
							</tr>
						</table>
						<p><input type="submit" name="select_email" value="Подтвердить" /></p>
					</form>
					
				</div>
			</div>
		<?endif;?>
	<?endif;?>
	
	<?if($_GET['reg_action'] == 'confirm_email' && !empty($_GET['check_code'])):?>
		<?
			CModule::IncludeModule('iblock');
			$bOk = false;
			global $USER;
			
			$rsCheck = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 11, "=PROPERTY_CHECK_CODE" => htmlspecialchars($_GET['check_id'])));
			if($obCheck = $rsCheck->GetNextElement())
			{
				//Пользователь найден
				$bOk = true;
				$arCheck = $obCheck->GetFields();
				$arCheck['PROPERTIES'] = $obCheck->GetProperties();
				
				$rsUser = CUser::GetList(
					($by = "email"),
					($order = "desc"),
					array(
						"ID" => (int)$arCheck['PROPERTIES']['USER_ID']['VALUE']
					),
					array(
						"SELECT" => array("UF_*")
					)
				);
				
				if($arUser = $rsUser->Fetch())
				{
					//Делаем активным нового пользователя
					if($arUser['ACTIVE'] == 'N')
					{
						$arUserFields['ACTIVE'] = 'Y';
					}
					//Добавляем в список соц сетей ту самую сеть
					if(!empty($arUser['UF_SOCIALS_ACCOUNTS']))
					{
						$arUserFields['UF_SOCIALS_ACCOUNTS'] = $arUser['UF_SOCIALS_ACCOUNTS'];
					}
					$arUserFields['UF_SOCIALS_ACCOUNTS'][] = $arCheck['PROPERTIES']['EXTERNAL_ID']['VALUE'];
					
					$user = new CUser;
					$result = $user->Update($arUser['ID'], $arUserFields);
					if(!$result)
					{
						$arError[] = 'Ошибка#3: Не удалось обновить данные пользователя';
					}
					else
					{
						//Удаляем запрос на подтверждение
						CIBlockElement::Delete((int)$arCheck['ID']);
						//Авторизовываем
						$USER->Authorize((int)$arUser['ID']);
						LocalRedirect("/?reg_action=confirmed");
					}
				}
				else
					$arError[] = 'Ошибка#2: Не найден пользователь  '.(int)$arCheck['PROPERTIES']['USER_ID']['VALUE'];
			}
			else
				$arError[] = 'Ошибка#1: Не найдено подтверждение';
		?>
		<?if(!empty($arError)):?>
			<script>
				$(document).ready(function(){

					$('#social_check_window').dialog({
						autoOpen: true,
						draggable: false,
						modal: true,
						resizable: false,
						width: 425,
						//show: 'scale',
						//hide: 'scale'
					});
					
					$("#social_check_close").click(function(){
						$('#social_check_window').dialog("close");
					});
				});
			</script>
			<div id="social_check_window" style="display:none;">
				<div id="social_check_close" class="modal_close_button"></div>
				<div id="social_check_body" style="padding:20px;">
					<p style="font-size: 18px; color:#444; text-align:center;">Ошибка</p>
					<?foreach($arError as $error):?>
						<p><?=$error?></p>
					<?endforeach;?>
				</div>
			</div>
		<?endif;?>
	<?endif;?>

	<?if($_GET['reg_action'] == 'confirmed'):?>
		 <script>
			$(document).ready(function(){

				$('#social_check_window').dialog({
					autoOpen: true,
					draggable: false,
					modal: true,
					resizable: false,
					width: 425,
					//show: 'scale',
					//hide: 'scale'
				});
				
				$("#social_check_close").click(function(){
					$('#social_check_window').dialog("close");
				});
			});
		</script>
		<div id="social_check_window" style="display:none;">
			<div id="social_check_close" class="modal_close_button"></div>
			<div id="social_check_body" style="padding:20px;">
				<p style="font-size: 18px; color:#444; text-align:center;">Аккаунт подтвержден!</p>
				<p>Ваш аккаунт успешно подтвержден!</p>
			</div>
		</div>
	<?endif;?>	
<?endif;?>