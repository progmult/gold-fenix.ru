<?php
	$url = $_SERVER['REQUEST_URI'];
	if(strpos($url, '?') !== FALSE)
		$url = strstr($url, '?', true);
	
	$arURI = explode('/', $url);
	$arURI = array_filter($arURI);
	
	if(in_array('catalog', $arURI) && count($arURI) >= 3)
	{
		echo '<link rel="canonical" href="http://'.$_SERVER['HTTP_HOST'].'/'.implode('/', array($arURI[1], $arURI[2])).'/" />';
	}
?>